import Foundation

enum HTTPStatus: Int {
    case success = 200
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case internalError = 500
    case urlError = 0
    case serialisationError = 1
    case deserialisationError = 2
    case unknown = 3
}

enum customError: String {
    case invalidUrl = "Invalid URL"
    case unableToParse = "Unable to parse JSON"
    case requestTimeOut = "Request Timeout"
}

enum HTTPMethod: String {
    case get    = "GET"
    case post   = "POST"
    case put    = "PUT"
    case delete = "DELETE"
}


protocol HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String)
}

class HTTP {
    
    fileprivate var task: URLSessionTask?
    fileprivate var delegate: HTTPDelegate?
    fileprivate var threadSwitcher: (HTTPStatus, AnyObject?, String) {
        get {
            return (HTTPStatus.unknown, nil, "")
        }
        set {
            task = nil // Remove the task.
            DispatchQueue.main.async { // Switch to main thread.
                self.delegate?.completed(withStatus: newValue.0, responseObject: newValue.1, andErrorMsg: newValue.2)
            }
        }
    }
    
    init(delegate: HTTPDelegate?) {
        self.delegate = delegate
    }
    
    func cancel() {
        guard let task = task else { return }
        task.cancel()
    }
    
    func request(ofType method: HTTPMethod,
                 toURL url: URL?,
                 withPararmeters parameter: AnyObject? = nil,
                 andHeaders headers: [String : String] = ["Content-Type" : "application/json","Accept":"application/json"],
                 shouldPassToken should: Bool = false) {
        
        guard let url = url else { threadSwitcher = (.urlError, nil, customError.invalidUrl.rawValue); return }
        print(url.absoluteString)
        
        var request = URLRequest(url: url)
        // Add request method.
        request.httpMethod = method.rawValue
        // Add Token.
        
        if should {
            
            guard let token = UserDefaults.standard.value(forKey: "token") as? String else {return}
            
            request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        // Add headers.
        for header in headers {
            request.addValue(header.value, forHTTPHeaderField: header.key)
        }
        // Add body.
        if let params = parameter {
            guard let bodyData = JSONStringify(withJSON: params) else { threadSwitcher = (.serialisationError, nil, customError.unableToParse.rawValue); return }
            request.httpBody = bodyData
        }
        // Make request.
        sendRequest(request: request)
    }}

// JSON Stringifier.
private extension HTTP {
    
    func JSONStringify(withJSON value: AnyObject, prettyPrinted: Bool = false) -> Data? {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        
        if JSONSerialization.isValidJSONObject(value) {
            do
            {
                return try JSONSerialization.data(withJSONObject: value, options: options)
            }
            catch { print("Couldn't serialize JSON.") }
        }
        return nil
    }
    
}

// Perform request.
private extension HTTP {
    
    func sendRequest(request: URLRequest) {
        
        task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            
            var status: Int = 500
            if let _res = response as? HTTPURLResponse {
                status = _res.statusCode
                if status == 201 || status == 204 { status = 200 }
            }
            
            if let _error = error {
                print("Some error occurred: \(_error.localizedDescription)")
                self.threadSwitcher = (HTTPStatus(rawValue: status) ?? .unknown, nil, _error.localizedDescription)
                return
            }
            
            guard let data  = data else {
                print("Request timed out.")
                self.threadSwitcher = (HTTPStatus(rawValue: status) ?? .unknown, nil, customError.requestTimeOut.rawValue)
                return
            }
            
            // Check if the token has expired.
            if status == 403 //&& UserDefaults.standard.bool(forKey: Keys.login) {
            {
                do
                {
                    if let result = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? Dictionary<String, AnyObject>,
                        let error = result["errors"] as? String,
                        error == "You provided an invalid authorization token." {
                        // Handle re authentice
                        return
                    }
                }
                catch {}
            }
            
            do
            {
                let result = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                self.threadSwitcher = (HTTPStatus(rawValue: status) ?? .unknown, result as AnyObject, customError.unableToParse.rawValue)
            }
            catch {
                let result = String(data: data, encoding: .utf8)
                print("Couldn't get JSON from data.", "Got this instead", result ?? "Nope not a String either.")
                self.threadSwitcher = (HTTPStatus(rawValue: status) ?? .deserialisationError, result as AnyObject, customError.unableToParse.rawValue)
                return
            }
        })
        task?.resume()
    }
}
