//
//  EndPoint.swift
//  SP
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

let baseURL = "https://surender.aphroecs.com"
let mapURL = "https://maps.googleapis.com/maps/api/geocode/json"

enum Path: String {
    
    case signup =             "/signup"
    case login =                "/login"
    
    case otpVerifyForMobilechange = "/user/otpverify"
    case otpVerify = "/otpverify"
    case forgetSendOTP = "/forget_password_otp"
    
    
    case sendOTP = "/send_otp"
    case sendOTPForMobileChange = "/update_mobile_otp"
    
    case changePassword = "/user/change_password"
    case newPassword = "/user/new_password"
    
    case getProfile = "/user/get_profiles"
    case updateProfile = "/user/update_profile"
    
    case fetchCategories = "/products/category"
    
    case updateAddress = "/user/update_address"
    case createAddress = "/user/store_address"
    case deleteAddress = "/user/del_address"
    case fetchAddress = "/user/get_address"
    case fetchCityAndLocalities = "/user/city_locality_list"
    case fetchCity = "/user/city_list"
    case fetchState = "/user/state_list"
    case fetchLocality = "/user/locality_list"
    case fetchTimeSlot = "/orders/timeslot"
    case updateDefaultAddress = "/user/update_address_default"
    case fetchDiscount = "/orders/discount_slot"
    case fetchOrderDates = "/orders/dates"
    
    case fetchOrders = "/orders"
    case fetchOrder = "/order"
    
    case helpAndSupport = "/user/query"

    case resendEmail = "/send_verify_email"
    case confirmPickup = "/orders/confirm_pickup"
}

struct Endpoint {
    var path: String
    var queryItems: [URLQueryItem]?
    var shouldPassToken: Bool
    
    init(withPath path: Path, andSubPath subPath: String = "", andQueryItems queryItems: [URLQueryItem]? = nil, shouldPassToken: Bool = false) {
        self.shouldPassToken = shouldPassToken
        if subPath.isEmpty {
            self.path = path.rawValue
        } else {
            self.path = path.rawValue + subPath
        }
        
        self.queryItems = queryItems
    }
    var base: String {
        return baseURL
    }
    
    var api: String {
        return "/smartwash/public/api"
    }
    
    var urlComponents: URLComponents {
       // var components = URLComponents(string: isOauth() ? baseURLWithCredentials: baseURL)!
       
        var components = URLComponents(string: base)!
        
        components.path = api + path
        
        if let queryItems = self.queryItems {
            components.queryItems = []
            components.queryItems?.append(contentsOf: queryItems)
        }
        return components
    }
    
    var url: URL? {
        return urlComponents.url
    }
}

struct mapsAPI {
    
    var queryItems: [URLQueryItem]?
    
    var baseurl: String {
        return mapURL
    }
    
    init(queryItems: [URLQueryItem]? = nil) {
        self.queryItems = queryItems }
    
    var urlComponents: URLComponents {
        
    var components = URLComponents(string: baseurl)!
    
        if let queryItems = self.queryItems {
            components.queryItems = []
            components.queryItems?.append(contentsOf: queryItems)
        }
        
    return components
    }
    }
