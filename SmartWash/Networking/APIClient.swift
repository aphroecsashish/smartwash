//
//  APIClient.swift
//  SP
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

class APIClient {
    
    var delegate: HTTPDelegate?
    private lazy  var httpClient: HTTP = {
        return HTTP(delegate: self.delegate)
    }()
    
    init(withDelegate delegate: HTTPDelegate? = nil) {
        self.delegate = delegate
    }
    
    func cancelRequest() {
        httpClient.cancel()
    }
    
    func login(withParameters params: [String:AnyObject]) {
        let endpoint = Endpoint(withPath: Path.login)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,
                           withPararmeters: params as AnyObject)
    }
    
    func signup(withParameters params: [String:AnyObject]) {
        let endpoint = Endpoint(withPath: Path.signup)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,
                           withPararmeters: params as AnyObject,andHeaders:["Content-Type" : "application/json"])
    }
    
    func sendOTP(withParameters params: [String:AnyObject],forMobileChange: Bool? = false) {
        
        if forMobileChange! {
            
            let endpoint = Endpoint(withPath: Path.sendOTPForMobileChange)
            
            httpClient.request(ofType: .post,
                               toURL: endpoint.url,
                               withPararmeters: params as AnyObject,shouldPassToken: true)
            
        } else {
        
        let endpoint = Endpoint(withPath: Path.sendOTP)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,
                           withPararmeters: params as AnyObject)
        }}
    
    
    func resendOTP(withParameters params: [String:AnyObject],forMobileChange: Bool? = false) {
        
        if forMobileChange! {
            
            let endpoint = Endpoint(withPath: Path.sendOTPForMobileChange)
            
            httpClient.request(ofType: .post,
                               toURL: endpoint.url,
                               withPararmeters: params as AnyObject,shouldPassToken: true)
            
        } else {
            
            let endpoint = Endpoint(withPath: Path.sendOTP)
            
            httpClient.request(ofType: .post,
                               toURL: endpoint.url,
                               withPararmeters: params as AnyObject)
        }}

    
    func verifyOTP(withParameters params: [String:AnyObject],forMobileChange: Bool? = false) {
        
        if forMobileChange! {
            let endpoint = Endpoint(withPath: Path.otpVerifyForMobilechange)
            
            httpClient.request(ofType: .post,
                               toURL: endpoint.url,
                               withPararmeters: params as AnyObject,shouldPassToken: true) }
        else {
        let endpoint = Endpoint(withPath: Path.otpVerify)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,
                           withPararmeters: params as AnyObject)
        }}

    func fetchProfile(token:String) {
        
        let endpoint = Endpoint(withPath: Path.getProfile)
        
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,withPararmeters: nil,shouldPassToken: true)
    }
    
    func updateProfile(params: [String:AnyObject]) {
        
        let endpoint = Endpoint(withPath: Path.updateProfile)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func fetchAddresses() {
        
        let endpoint = Endpoint(withPath: Path.fetchAddress)
        
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func deleteAddress(withParameters id: String) {
        
        let endpoint = Endpoint(withPath: Path.deleteAddress,andSubPath: id)
        
        httpClient.request(ofType: .delete,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func fetchCityAndLocality(){
        
        let endPoint = Endpoint(withPath: Path.fetchCityAndLocalities)
        httpClient.request(ofType: .get, toURL: endPoint.url,shouldPassToken: true)
        
    }
    
    func getAddress(data: [String:AnyObject]) {
        
        let base = mapsAPI(queryItems: [])
        let url = URL(string: base.baseurl)
        httpClient.request(ofType: .get, toURL: url)
    }
    
    func changePassword(params: [String:AnyObject]) {
        
        let endpoint = Endpoint(withPath: Path.updateProfile)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func createAddress(params: [String:AnyObject]) {
    
    let endpoint = Endpoint(withPath: Path.createAddress)
    
    httpClient.request(ofType: .post,
    toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func fetchCategories() {
        let endpoint = Endpoint(withPath: Path.fetchCategories)
        
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func updateAddress(params: [String:AnyObject]) {
        
        let endpoint = Endpoint(withPath: Path.updateAddress)
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func updateDefaultAddress(params: [String:AnyObject]) {
        let endpoint = Endpoint(withPath: Path.updateDefaultAddress)
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func fetchdates(){
        let endpoint = Endpoint(withPath: Path.fetchOrderDates)
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func fetchTimeSlot(params: [String:AnyObject]) {
        let endpoint = Endpoint(withPath: Path.fetchTimeSlot)
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func fetchDiscounts(){
        let endpoint = Endpoint(withPath: Path.fetchDiscount)
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func fetchOrders(userID: Int) {
        let endpoint = Endpoint(withPath: Path.fetchOrders,andSubPath: "/\(userID)")
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func fetchOrder(orderID: Int) {
        let endpoint = Endpoint(withPath: Path.fetchOrder,andSubPath: "/\(orderID)")
        httpClient.request(ofType: .get,
                           toURL: endpoint.url,shouldPassToken: true)
    }
    
    func confirmPickup(express: Bool = true, userID: Int,requestedPickupDate: String,addressID: Int, timeSlot: Int, finalAmount: Double? = 0.0,gst: Double? = 0.0,discount: Double? = 0.0,orderAmount: Double? = 0.0 ,products:[[String:AnyObject]]? = []) {
        
        var params: [String:AnyObject] = [:]
        
        if express {
            params = ["customer_id": userID,"requested_pickup_date":requestedPickupDate,"address_id":addressID,"pickup_time":timeSlot] as [String:AnyObject]
        }
        else {
            
            params = ["customer_id": userID,"requested_pickup_date":requestedPickupDate,"address_id":addressID,"pickup_time":timeSlot,"final_amount":finalAmount!,"gst":gst!,"discount_amount":discount!,"order_amount":orderAmount!,"products":products!] as [String:AnyObject]
        }
        
        let endpoint = Endpoint(withPath: Path.confirmPickup)
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
        
    }
    
    func helpAndSupport(params: [String:AnyObject]) {
        
        let endpoint = Endpoint(withPath: Path.helpAndSupport)
        
        httpClient.request(ofType: .post,
                           toURL: endpoint.url,withPararmeters: params as AnyObject,shouldPassToken: true)
    }
    
    func resendEmailVerification(){
        let endPoint = Endpoint(withPath: Path.resendEmail)
        
        httpClient.request(ofType: .get,
                           toURL: endPoint.url,shouldPassToken: true)
    }
}

