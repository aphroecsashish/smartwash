//
//  OrderDetailsTableViewCell.swift
//  SmartWash
//
//  Created by AphroECS on 16/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class OrderDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var deliveryImage: UIImageView!
    @IBOutlet weak var lblSmartwashAddress: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var lblOrderDeliveredOn: UILabel!
    @IBOutlet weak var lblStepTwo: UILabel!
    @IBOutlet weak var lblStatusTop: UILabel!
    @IBOutlet weak var lblItemCountAndPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(withObject dData:[String:AnyObject]){
      
        var itemsTopDescription = ""
        
        if let products = dData["order_detail_products"] as? [[String:AnyObject]]
        {
            itemsTopDescription.append( " | " + String(describing: products.count) + " Items")
            
            if let finalAmount = dData["final_amount"] as? String {
                
                itemsTopDescription.append(contentsOf: " , ")
                itemsTopDescription.append( "Rs." + finalAmount)
                self.lblItemCountAndPrice.text = itemsTopDescription
            }
            
            if products.count == 0 {
                self.lblItemCountAndPrice.text = "Express Pickup"
            }
            
        }
        
        if let step1 = dData["track_order_step_one"] as? String {
            self.lblUserAddress.text = step1
        }
        
        if let step2 = dData["track_order_step_two"] as? String {
            self.lblStepTwo.text = step2
        }
        
        if let step3 = dData["track_order_step_three"] as? String {
            self.lblSmartwashAddress.text = step3
        }
        
        if let userLocation = dData["user_locations"] as? [String:AnyObject] {
            
        }
        
        if let orderStatus = dData["status"] as? String {
            
            if orderStatus == "PLACED" {
                deliveryImage.image = #imageLiteral(resourceName: "location-locator2")
            }
            else if orderStatus == "PICKED" {
                                    deliveryImage.image = #imageLiteral(resourceName: "location-locator")
            }
            else {
                                deliveryImage.image = #imageLiteral(resourceName: "location-locator3")
            }
            
            self.lblStatusTop.text = orderStatus
            self.lblOrderDeliveredOn.text = orderStatus
        }
    }

}
