//
//  HeaderTableViewCell.swift
//  TestTable
//
//  Created by apple on 05/09/18.
//  Copyright © 2018 Minersinc. All rights reserved.
//

import UIKit

protocol SubHeaderTableViewCellDelegate {
    
    func expandTapped(atIndex index: Int)
    
}

class HeaderTableViewCell: UITableViewCell {

    var delegate: SubHeaderTableViewCellDelegate?
    
    @IBOutlet weak var cellTapGesture: UITapGestureRecognizer!
    @IBOutlet weak var headerTitle: UILabel!
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var expandBtn: UIButton!
    
    @IBAction func expandTapped(_ sender: UIButton) {
        delegate?.expandTapped(atIndex: sender.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
