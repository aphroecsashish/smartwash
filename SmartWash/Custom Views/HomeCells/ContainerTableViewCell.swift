//
//  ContainerTableViewCell.swift
//  TestTable
//
//  Created by apple on 05/09/18.
//  Copyright © 2018 Minersinc. All rights reserved.
//

import UIKit

protocol ContainerTableViewCellDelegate: NSObjectProtocol {
    func expand()
}

class ContainerTableViewCell: UITableViewCell {

    let manager = CoreDataManager.sharedManager
    
    var selectedServiceType: ServiceType?
    
    @IBOutlet weak var tableView: UITableView!
    
    var category: Category!
    var containerSuperIndex: IndexPath!
    weak var delegate: ContainerTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.register(SubHeaderTableViewCell.self)
        tableView.register(ListTableViewCell.self)
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
}

extension ContainerTableViewCell: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return category.subCategories.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return category.subCategories[section].expanded ? category.subCategories[section].products.count : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard category.expanded else {
            return nil
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SubHeaderTableViewCell") as? SubHeaderTableViewCell{
            cell.btn.addTarget(self, action: #selector(tap(_ :)), for: .touchUpInside)
            cell.btn.tag = section
            let cellDAta = category.subCategories[section]
            cell.cellImage.sd_setImage(with: cellDAta.image.toURL, placeholderImage: #imageLiteral(resourceName: "user(7)"), options: [.progressiveDownload,.continueInBackground], completed: nil)
            cell.title.text = cellDAta.name
            cell.subtitle.text = cellDAta.subtitle
            return cell.contentView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return category.expanded ? 70: 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as! ListTableViewCell
            
            cell.addBtn.tag = indexPath.row
            cell.decreaseBtn.tag = indexPath.row
            cell.increaseBtn.tag = indexPath.row
            cell.delegate = self
            cell.isFromCart = false
            cell.categoryType.isHidden = true
            cell.section = indexPath.section
            cell.selectedServiceType = self.selectedServiceType
        
        let product = category.subCategories[indexPath.section].products[indexPath.row]
        
        for price in product.productPrices {
            if price.price == 0.0 && price.serviceType == self.selectedServiceType {
                return UITableViewCell()
            }
        }
        
        cell.initData(product: product)
        return cell
    }
    
    @objc func tap(_ sender: UIButton) {
       category.subCategories[sender.tag].expanded = !category.subCategories[sender.tag].expanded
        delegate?.expand()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let product = category.subCategories[indexPath.section].products[indexPath.row]
        
        for price in product.productPrices {
            if price.price == 0.0 && price.serviceType == self.selectedServiceType {
                return 0
            }
        }
        return 70
    }
}

extension ContainerTableViewCell: ListTableViewCellDelegate {
    func shouldReload(shouldReload reload: Bool) {
        self.tableView.reloadData()
    }
    
    func valueChanged(forProduct updatedProduct: Product, section: Int, row: Int, shouldRemove: Bool? = false) {
        
      //  updatedProduct.superCategory = self.category.name
        
        category.subCategories[section].products[row] = updatedProduct
        
        manager.updateCart(product: updatedProduct, serviceType: self.selectedServiceType!, shouldRemove: shouldRemove!)
    }
}
