//
//  SubHeaderTableViewCell.swift
//  TestTable
//
//  Created by apple on 05/09/18.
//  Copyright © 2018 Minersinc. All rights reserved.
//

import UIKit

class SubHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btn: UIButton!
    
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        let hasContentView = self.subviews.contains(self.contentView)
        if (hasContentView) {
            self.contentView.removeFromSuperview()
        }
    }
    
}
