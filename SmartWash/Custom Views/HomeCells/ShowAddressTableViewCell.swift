//
//  ShowAddressTableViewCell.swift
//  SmartWash
//
//  Created by AphroECS on 01/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class ShowAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var AddressLabel: UILabel!
    
    @IBOutlet weak var chekImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
      
        if selected {
        super.setSelected(selected, animated: animated)
        self.layer.borderWidth = 0.5
        self.chekImage.isHidden = false
        self.layer.cornerRadius = 3
            self.contentView.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        self.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
        else {
            self.chekImage.isHidden = true
            self.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.layer.borderWidth = 0
        }
    }
}
