//
//  ListTableViewCell.swift
//  TestTable
//
//  Created by apple on 05/09/18.
//  Copyright © 2018 Minersinc. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData

protocol ListTableViewCellDelegate {
    func valueChanged(forProduct updatedProduct: Product, section: Int, row: Int,shouldRemove: Bool?)
    func shouldReload(shouldReload reload: Bool)
}

class ListTableViewCell: UITableViewCell {
    
    var isFromCart: Bool = false
    
    var selectedServiceType: ServiceType?
    
    @IBOutlet weak var categoryType: UILabel!
    
    var delegate: ListTableViewCellDelegate?
    
    @IBOutlet weak var rsLabel: UILabel!
    var cellProduct: Product!
    
    var section: Int = 0
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var decreaseBtn: UIButton!
    
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var cellTitle: UILabel!
    
    @IBOutlet weak var itemsActionView: UIView!
    
    @IBOutlet weak var increaseBtn: UIButton!
    
    @IBOutlet weak var itemsLabel: UILabel!
    
    @IBOutlet weak var addBtn: DesignableButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initData(product: Product){
        
        self.cellProduct = product
        
        if self.isFromCart  {
            self.cellTitle.text = self.cellProduct.name + " - " + self.cellProduct.superCategory
        }
        else {
            self.cellTitle.text = self.cellProduct.name
        }
       
        if cellProduct.serviceType != nil {
            self.categoryType.text = self.selectedServiceType?.rawValue
            
            if self.selectedServiceType?.rawValue == "WASH_IRON" {
                    self.categoryType.text = (self.selectedServiceType?.rawValue.replacingOccurrences(of: "_", with: " & ").lowercased() ?? "")
            }
            else {
                self.categoryType.text = (self.selectedServiceType?.rawValue.replacingOccurrences(of: "_", with: " ").lowercased() ?? "" )
            }
        }
        
        setupPrice(service: self.selectedServiceType!)
        
        getCart(product: self.cellProduct, serviceType: self.selectedServiceType!)
        
        setupItemValues()
    }

    func setupPrice(service: ServiceType) {
        
        if !cellProduct.productPrices.isEmpty
        {
        for price in cellProduct.productPrices {
            
            if price.serviceType == service {
                
                if price.price == 0.0 {
                    self.priceLabel.text = "Service not available"
                    rsLabel.isHidden = true
                    self.addBtn.isHidden = true
                    self.addBtn.isEnabled = false
                }
                else {
                self.priceLabel.text = String(describing: price.price)
                    rsLabel.isHidden = false
                    self.addBtn.isHidden = false
                    self.addBtn.isEnabled = true
                }
            }
        }}
        else {
            self.priceLabel.text = String(describing: cellProduct.price)
        }
        self.setNeedsLayout()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func decreaseTapped(_ sender: UIButton) {
        
        if self.selectedServiceType! == .washIron
        {
            if cellProduct.initialValueWash > 0 {
                cellProduct.initialValueWash -= 1
            }
         if cellProduct.initialValueWash == 0 {
            
            self.cellProduct.initialValueWash = 0
            self.itemsActionView.isHidden = true
            self.addBtn.isHidden = false
            self.decreaseBtn.isEnabled = false
        }
        self.itemsLabel.text = String(describing: cellProduct.initialValueWash)
        }
            
            
        else if self.selectedServiceType! == .dryClean {
            
            if cellProduct.initialValueDryClean > 0 {
                cellProduct.initialValueDryClean -= 1
            }
             if cellProduct.initialValueDryClean == 0 {
                self.cellProduct.initialValueDryClean = 0
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
                self.decreaseBtn.isEnabled = false
            }
            self.itemsLabel.text = String(describing: cellProduct.initialValueDryClean)
        }
            
            
        else if self.selectedServiceType! == .iron {
            
            if cellProduct.initialValueIron > 0 {
                cellProduct.initialValueIron -= 1
            }
             if cellProduct.initialValueIron == 0 {
                self.cellProduct.initialValueIron = 0
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
                self.decreaseBtn.isEnabled = false
            }
            self.itemsLabel.text = String(describing: cellProduct.initialValueIron)
        }
            
        else if self.selectedServiceType! == .petrolWash {
            
            if cellProduct.initialValuePetrolWash > 0 {
                cellProduct.initialValuePetrolWash -= 1
            }
             if cellProduct.initialValuePetrolWash == 0 {
                self.cellProduct.initialValuePetrolWash = 0
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
                self.decreaseBtn.isEnabled = false
            }
            self.itemsLabel.text = String(describing: cellProduct.initialValuePetrolWash)
        }

        delegate?.valueChanged(forProduct: self.cellProduct, section: self.section, row: sender.tag,shouldRemove: true)
        
        }
    
    @IBAction func addBtnTapped(_ sender: UIButton) {
        self.addBtn.isHidden = true
        self.itemsActionView.isHidden = false
        
        if self.selectedServiceType! == .washIron
        {
        cellProduct.initialValueWash += 1
        self.decreaseBtn.isEnabled = true
        self.itemsLabel.text = String(describing: cellProduct.initialValueWash)
        }
        
        else if self.selectedServiceType! == .dryClean
        {
            cellProduct.initialValueDryClean += 1
            self.decreaseBtn.isEnabled = true
            self.itemsLabel.text = String(describing: cellProduct.initialValueDryClean)
        }
        
        else if self.selectedServiceType! == .iron
        {
            cellProduct.initialValueIron += 1
            self.decreaseBtn.isEnabled = true
            self.itemsLabel.text = String(describing: cellProduct.initialValueIron)
        }
        
        else if self.selectedServiceType! == .petrolWash
        {
            cellProduct.initialValuePetrolWash += 1
            self.decreaseBtn.isEnabled = true
            self.itemsLabel.text = String(describing: cellProduct.initialValuePetrolWash)
        }
        
        delegate?.valueChanged(forProduct: self.cellProduct, section: self.section, row: sender.tag, shouldRemove: false)
        
    }
    
    @IBAction func increaseTapped(_ sender: UIButton) {
        
        if self.selectedServiceType! == .washIron
        {
        cellProduct.initialValueWash += 1
        if cellProduct.initialValueWash > 0 {
            self.decreaseBtn.isEnabled = true
        }
        self.itemsLabel.text = String(describing: cellProduct.initialValueWash)
        }
    
        else if self.selectedServiceType! == .dryClean
        {
            cellProduct.initialValueDryClean += 1
            if cellProduct.initialValueDryClean > 0 {
                self.decreaseBtn.isEnabled = true
            }
            self.itemsLabel.text = String(describing: cellProduct.initialValueDryClean)
        }
        
        else if self.selectedServiceType! == .iron
        {
            cellProduct.initialValueIron += 1
            if cellProduct.initialValueIron > 0 {
                self.decreaseBtn.isEnabled = true
            }
            self.itemsLabel.text = String(describing: cellProduct.initialValueIron)
        }
        
        else if self.selectedServiceType! == .petrolWash
        {
            cellProduct.initialValuePetrolWash += 1
            if cellProduct.initialValuePetrolWash > 0 {
                self.decreaseBtn.isEnabled = true
            }
            self.itemsLabel.text = String(describing: cellProduct.initialValuePetrolWash)
        }
        
        delegate?.valueChanged(forProduct: self.cellProduct, section: self.section, row: sender.tag, shouldRemove: false)
    }
    
    func getCart(product: Product,serviceType: ServiceType) {
        
        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        let fetchRequestSender = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        
        let predicateProductID = NSPredicate(format: "productId == %@", String(describing: product.id))
        let predicateServiceType = NSPredicate(format: "serviceType == %@", serviceType.rawValue)
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [predicateProductID, predicateServiceType])
        
        fetchRequestSender.returnsObjectsAsFaults = false
        
        fetchRequestSender.predicate = andPredicate
        
        do {
            guard let result = try managedContext.fetch(fetchRequestSender) as? [NSManagedObject] else { return }
                
                guard let count = result.first?.value(forKey: "count") as? Int else {
                    switch serviceType {
                    case .washIron:
                        self.cellProduct.initialValueWash = 0
                    case .dryClean:
                        self.cellProduct.initialValueDryClean = 0
                    case .petrolWash:
                        self.cellProduct.initialValuePetrolWash = 0
                    case .iron:
                        self.cellProduct.initialValueIron = 0
                    }
                    return
                }
            
            if count > 0 {
            switch serviceType {
            case .washIron:
                self.cellProduct.initialValueWash = count
            case .dryClean:
                self.cellProduct.initialValueDryClean = count
            case .petrolWash:
                self.cellProduct.initialValuePetrolWash = count
            case .iron:
                self.cellProduct.initialValueIron = count
            }
            self.decreaseBtn.isEnabled = true
            }}
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func setupItemValues() {
        
        if self.selectedServiceType! == .washIron
        {
            if self.cellProduct.initialValueWash == 0 {
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
            }
            else {
                self.itemsLabel.text = String(describing: self.cellProduct.initialValueWash)
                self.itemsActionView.isHidden = false
                self.addBtn.isHidden = true
            }
        }
        
        else if self.selectedServiceType! == .iron
        {
            if self.cellProduct.initialValueIron == 0 {
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
            }
            else {
                          self.itemsLabel.text = String(describing: self.cellProduct.initialValueIron)
                self.itemsActionView.isHidden = false
                self.addBtn.isHidden = true
            }
        }
        
        else if self.selectedServiceType! == .dryClean
        {
            if self.cellProduct.initialValueDryClean == 0 {
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
            }
            else {
                          self.itemsLabel.text = String(describing: self.cellProduct.initialValueDryClean)
                self.itemsActionView.isHidden = false
                self.addBtn.isHidden = true
            }
        }
        
        else if self.selectedServiceType! == .petrolWash
        {
            if self.cellProduct.initialValuePetrolWash == 0 {
                self.itemsActionView.isHidden = true
                self.addBtn.isHidden = false
            }
            else {
                self.itemsLabel.text = String(describing: self.cellProduct.initialValuePetrolWash)
                self.itemsActionView.isHidden = false
                self.addBtn.isHidden = true
            }
        }
        
    }
    
}
