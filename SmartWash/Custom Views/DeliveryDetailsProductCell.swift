//
//  DeliveryDetailsProductCell.swift
//  SmartWash
//
//  Created by AphroECS on 18/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class DeliveryDetailsProductCell: UITableViewCell {
    
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var serviceType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
