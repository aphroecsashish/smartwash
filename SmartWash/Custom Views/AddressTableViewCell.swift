//
//  AddressTableViewCell.swift
//  SmartWash
//
//  Created by AphroECS on 14/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

protocol AddressTableViewCellDelegate {
    
    func editTapped(atIndex: Int)
    func deleteTapped(atIndex: Int)
    func makeDefaultTapped(atIndex: Int)
    
}

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var cellTickImage: UIImageView!
    @IBOutlet weak var defaultBtn: UIButton!
    
    var delegate: AddressTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    @IBAction func editBtnTapped(_ sender: UIButton) {
        delegate?.editTapped(atIndex: sender.tag)
    }
    
    @IBAction func deleteTapped(_ sender: UIButton) {
        delegate?.deleteTapped(atIndex: sender.tag)
    }
    
    @IBAction func defaultTapped(_ sender: UIButton) {
        delegate?.makeDefaultTapped(atIndex: sender.tag)
    }
}
