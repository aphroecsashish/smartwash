//
//  ItemCollectionViewCell.swift
//  SmartWash
//
//  Created by AphroECS on 30/08/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
}
