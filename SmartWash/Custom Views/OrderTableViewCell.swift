//
//  OrderTableViewCell.swift
//  SmartWash
//
//  Created by Ashish sharma on 15/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblOrderID: UILabel!

    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblOrderStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(withOrder order: Order){
        
        self.lblOrderID.text = "Order Id :" + String(describing: order.id)
        self.lblAddress.text = order.address
        self.lblDate.text = order.order_Date
        
        if order.status == "cancelled" {
        self.lblOrderStatus.text = order.status
        } else {
                self.lblOrderStatus.text = ""
        }
    }
}
