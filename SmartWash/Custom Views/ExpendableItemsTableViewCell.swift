//
//  ExpendableItemsTableViewCell.swift
//  SmartWash
//
//  Created by AphroECS on 04/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class ExpendableItemsTableViewCell: UITableViewCell {

    var numberOfRows = 4
    
    @IBOutlet weak var expendableTabel: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.expendableTabel.delegate = self
        self.expendableTabel.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension ExpendableItemsTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        
        cell.backgroundColor = #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width, height: 50)
        
    }
    
}
