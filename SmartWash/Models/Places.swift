//
//  Places.swift
//  SmartWash
//
//  Created by AphroECS on 18/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

struct Places {
    let id: Int
    let name: String
    let stateID: Int
    var localities: [Locality]
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case stateID = "state_id"
        case localities
    }
    
    init(withJSON json: [String:AnyObject]) {
        
        id = (json["id"] as? Int)!
        name = (json["name"] as? String)!
        stateID = (json["state_id"] as? Int)!
        
        localities = []
        
        for js in (json["localities"] as? [[String: AnyObject]])! {
            
            let loc = Locality.init(withJSON: js)
            localities.append(loc)
            
        }
    }
}

struct Locality: Codable {
    let id: Int
    let name: String
    let cityID: Int
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case cityID = "city_id"
    }
    
    init(withJSON json: [String:AnyObject]){
        id = (json["id"] as? Int)!
        name = (json["name"] as? String)!
        cityID = (json["city_id"] as? Int)!
    }
    
}
