//
//  Orders.swift
//  SmartWash
//
//  Created by AphroECS on 12/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

struct Order {
    var id: Int
    var order_Date: String
    var address: String
    var status: String
    
    init(withJson json: [String:AnyObject]) {
        
        id = json["id"] as? Int ?? 0
        order_Date = json["order_date"] as? String ?? ""
        address = json["address"] as? String ?? ""
        status = json["status"] as? String ?? ""
    }
}
