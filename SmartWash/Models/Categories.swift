//
//  Categories.swift
//  SmartWash
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation
import CoreData

class CategoryList {
    var categories: [Category]
    
    init(withJSON json: [[String:AnyObject]]){
        var categorie: [Category] = []
        for cat in json {
            let category = Category.init(withJSON: cat)
            categorie.append(category)
        }
        self.categories = categorie
    }
}

class Category {
    let id: Int
    let name, subtitle: String
    let status: Int
    var expanded: Bool = false
    var subCategories: [Category] = []
    let categoryID: Int?
    var products: [Product] = []
    var image: String
    
    init(withJSON json: [String:AnyObject]) {
        
        self.id = json["id"] as? Int ?? 0
        self.name = json["name"] as? String ?? ""
        self.status = json["status"] as! Int
        self.categoryID = (json["category_id"] as? Int)
        self.products = []
        self.subCategories = []
        
        if let subCategory = json["sub_categories"] as? [[String:AnyObject]]  {
            for cat in subCategory {
                self.subCategories.append(Category.init(withJSON: cat))
            }
        }
        else { self.subCategories = [] }
       
        
        if let products = json["products"] as? [[String:AnyObject]] {
            for prod in products {
                self.products.append(Product.init(withJSON: prod,superCategory: self.name))
            }
        } else {
            self.products = []
        }
        
        
        if let subtitle = json["products_list"] as? String {
            self.subtitle = subtitle
        } else {
            self.subtitle = ""
        }
        
        self.image = ""
        guard let url = json["image"] as? String else {
        return}
        self.image = url
    }
}

class Product {
    let id: Int
    let name: String
    var superCategory: String
    let categoryID, subCategoryID: Int
    let price: Double
    let status: Int
    var productPrices: [ProductPrice]
    var initialValueWash = 0
    var initialValueDryClean = 0
    var initialValuePetrolWash = 0
    var initialValueIron = 0
    var count: Int?
    var serviceType: ServiceType?
    
    var isWashAvailable: Bool?
    var isDryAvailable: Bool?
    var isPetrolAvailable: Bool?
    var isIronAvailable: Bool?
    
    init(withJSON json: [String:AnyObject], count: Int? = 0,superCategory: String) {
        
        self.id = json["id"] as? Int ?? 0
        self.categoryID = json["category_id"] as? Int ?? 0
        self.subCategoryID = json["sub_category_id"] as? Int ?? 0
        self.status = json["status"] as? Int ?? 0
        self.name = json["name"] as? String ?? ""
        self.price = json["price"] as?  Double ?? 0.0
        self.superCategory = superCategory
        self.productPrices = []
        
        if let pricesArray = json["product_prices"] as? [[String:AnyObject]] {
            
            for price in pricesArray {
                self.productPrices.append(ProductPrice.init(withJSON: price ))
            }
        }
        
        self.isDryAvailable = json["DRY_CLEAN"] as! Bool
        self.isIronAvailable = json["IRON"] as! Bool
        self.isWashAvailable = json["WASH_IRON"] as! Bool
        self.isPetrolAvailable = json["PETROL_WASH"] as! Bool
        
    }
    
    init(withNSManaged obj: NSManagedObject){
            
            self.id = obj.value(forKey: "productId") as? Int ?? 0
            self.categoryID = obj.value(forKey: "categoryId") as? Int ?? 0
            self.price = obj.value(forKey: "price") as? Double ?? 0.0
            self.count = obj.value(forKey: "count") as? Int ?? 0
            self.name = obj.value(forKey: "productName") as? String ?? ""
            self.subCategoryID = obj.value(forKey: "subCategoryId") as? Int ?? 0
        
            self.superCategory = obj.value(forKey: "superCategory") as? String ?? ""
        
            self.serviceType = ServiceType.init(rawValue: obj.value(forKey: "serviceType") as? String ?? "")
        
            self.productPrices = []
            self.status = 0
    }
}

class ProductPrice {
    let id, productID: Int
    let serviceType: ServiceType
    let status: Int
    let price: Double

    init(withJSON json: [String:AnyObject]) {
        
        self.id = json["id"] as! Int
        self.productID = json["product_id"] as! Int
        self.serviceType = ServiceType.init(rawValue: (json["service_type"] as! String))!
        self.status = json["status"] as! Int
        
        if let price = json["price"] as? Double {
            self.price = price
        }
        else if let price = json["price"] as? Int {
            self.price = Double(price)
        }
        else {
            self.price = (json["price"] as! String).toDouble
        }
    }}

enum ServiceType: String {
    case dryClean = "DRY_CLEAN"
    case iron = "IRON"
    case petrolWash = "PETROL_WASH"
    case washIron = "WASH_IRON"
}
