//
//  Profile.swift
//  SmartWash
//
//  Created by AphroECS on 12/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

struct Response {
    let status: String
    var profile: Profile
    
    init (withData data: [String: AnyObject]){
        
        status = data["status"] as? String ?? ""
        let profileJson = data["data"] as? [String:AnyObject] ?? [:]
        profile = Profile.init(withJSON: profileJson)
    }
}

struct Profile {
    let mobile, name,email,userID, mac,createdAt, updatedAt: String
    let status, admin,id: Int
    let verified: Bool
    
    
    init(withJSON json: [String:AnyObject]){
        
        id = json["id"] as? Int ?? 0
        email = json["email"] as? String ?? ""
        name = json["name"] as? String ?? ""
        mobile = json["mobile"] as? String ?? ""
        mac = json["mac"] as? String ?? ""
        status = json["status"] as? Int ?? 0
        admin = json["admin"] as? Int ?? 0
        createdAt = json["created_at"] as? String ?? ""
        updatedAt = json["updated_at"] as? String ?? ""
        self.userID = json["userID"] as? String ?? ""
        self.verified = json["email_verified"] as? Bool ?? false
    }
    
}

// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
