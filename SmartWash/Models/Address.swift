//
//  Address.swift
//  SmartWash
//
//  Created by AphroECS on 12/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

struct Address: Codable {
    let id, userID, isDefault,cityID, LocalityId : Int
    let house, locality, zipCode, city, fulladdress,addressOne: String
    
    init(withJSON data: [String:AnyObject]) {
       
        id = data["id"] as? Int ?? 0
        userID = data["user_id"] as? Int ?? 0
        house = data["house"] as? String ?? ""
        locality = data["locality"] as? String ?? ""
        zipCode = data["zip_code"] as? String ?? ""
        city = data["city"] as? String ?? ""
        isDefault = data["default_address"] as? Int ?? 0
        fulladdress = data["complete_address"] as? String ?? ""
        cityID = data["city_id"] as? Int ?? 0
        LocalityId = data["locality_id"] as? Int ?? 0
        addressOne = data["address_one"] as? String ?? ""
    }
}

struct DateSlot: Codable {
    let date, viewDate: String
    
    init(withJSON data: [String:AnyObject]) {
        date = data["date"] as? String ?? ""
        viewDate = data["view_date"] as? String ?? ""
    }
    
}

struct TimeSlot: Codable {
    let id: Int
    let timeSlot: String
    
    init(withJSON data: [String:AnyObject]) {
        id = data["id"] as? Int ?? 0
        timeSlot = data["time_slot"] as? String ?? ""
    }
    
}
