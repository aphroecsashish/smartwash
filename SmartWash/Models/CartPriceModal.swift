//
//  CartPriceModal.swift
//  SmartWash
//
//  Created by Ashish sharma on 11/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

struct DiscountResponse {
    
    let status: String
    let gst: Double
    var discountData: [DiscountSlabs]
    
    init(withJson json: [String:AnyObject]) {
        
        self.status = json["status"] as! String
        self.gst = json["gst"] as? Double ?? 0.0
        self.discountData = []
        
        guard let discountSlabs = json["data"] as? [[String:AnyObject]] else {return}
        
        for slab in discountSlabs {
            
            let sl = DiscountSlabs(withJson: slab)
            self.discountData.append(sl)
        }
    }
}

struct DiscountSlabs {
    let id, slabFrom, slabTo, discount,status : Int
    
    init(withJson json: [String:AnyObject]) {
        
        self.id = json["id"] as! Int
        self.slabFrom = json["slab_from"] as! Int
        self.discount = json["discount"] as! Int
        self.slabTo = json["slab_to"] as! Int
        self.status = json["status"] as! Int
        }
}
