//
//  File.swift
//  SmartWash
//
//  Created by AphroECS on 01/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation
import CoreData

protocol CoreDataManagerDelegate {
    func cartUpdated(withProduct: Product)
    func cartCleared()
}

class CoreDataManager: NSObject {
    
    var delegate: CoreDataManagerDelegate?
    
    static let sharedManager = CoreDataManager.init()
    
    private override init() {
    }
    
    lazy var persistantContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "SmartWash")
        
        container.loadPersistentStores(completionHandler: { (NSPersistentStoreDescription, error) in
            
            if let foundError = error as? NSError {
                print(foundError)
            }
        })
        return container
    }()
    
    func saveContext(){
        
        let context = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        if context.hasChanges {
            do {
                try context.save()
            }
            catch {
            }
        }
    }
    
    func insertProduct(product: Product,serviceType: ServiceType) {
        
        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Cart", in: managedContext)
        
        let newProduct = NSManagedObject(entity: entity!, insertInto: managedContext)
        
        var price: Double = 0.0
        
        for priceX in product.productPrices {
            
            if priceX.serviceType == serviceType {
                price = priceX.price
                continue
            }
        }
        
        newProduct.setValue(product.superCategory, forKey: "superCategory")
        newProduct.setValue(product.id, forKey: "productId")
        newProduct.setValue(product.categoryID
            , forKey: "categoryId")
        newProduct.setValue(price, forKey: "price")
        newProduct.setValue(1, forKey: "count")
        newProduct.setValue(product.name, forKey: "productName")
        newProduct.setValue(product.subCategoryID, forKey: "subCategoryId")
        newProduct.setValue(serviceType.rawValue, forKey: "serviceType")
        do {
            try managedContext.save()
            self.delegate?.cartUpdated(withProduct: product)
        }
        catch {
            if let error = error as? NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    func getCartFromDB() -> [NSManagedObject] {
        
        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        let cartFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        
        cartFetch.returnsObjectsAsFaults = false
        
        do {
           guard let cartResult = try managedContext.fetch(cartFetch) as? [NSManagedObject]
            else {
                return []
            }
            return cartResult
        
        } catch {
            fatalError("Failed to fetch products: \(error)")
        }
    }
    
    func updateCart(product: Product,serviceType: ServiceType, shouldRemove: Bool? = false) {
        
        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        let fetchRequestSender = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        
        let predicateProductID = NSPredicate(format: "productId == %@", String(describing: product.id))
        let predicateServiceType = NSPredicate(format: "serviceType == %@", serviceType.rawValue)
        let andPredicate = NSCompoundPredicate(type: .and, subpredicates: [predicateProductID, predicateServiceType])
        
        fetchRequestSender.returnsObjectsAsFaults = false
        
        fetchRequestSender.predicate = andPredicate
        
        do {
            guard let result = try managedContext.fetch(fetchRequestSender) as? [NSManagedObject] else { return }
            
            if result.isEmpty && !(shouldRemove!) {
                    insertProduct(product:product,serviceType: serviceType) }
            else {
                
                guard let count = result.first?.value(forKey: "count") as? Int else {
                    return
                }
                    if shouldRemove! {
                        if count == 0 || count == 1 {
                             result.first?.setValue(0, forKey: "count")
                            managedContext.delete(result.first!)
                        }
                        else {
                            result.first?.setValue(count - 1, forKey: "count")
                        }
                    }
                    else {
                result.first?.setValue(count + 1, forKey: "count")
                    }
                do {
                    try managedContext.save()
                    self.delegate?.cartUpdated(withProduct: product)
                }
            }
        }
        catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getCartCount() -> Int {
        
        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        let fetchRequestSender = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        
        fetchRequestSender.returnsObjectsAsFaults = false
        
        do {
            guard let result = try managedContext.fetch(fetchRequestSender) as? [NSManagedObject] else { return 0 }
            
            if result.isEmpty {
                return 0 }
            else {
                
                var count = 0
                
                for res in result {
                    guard let countx = res.value(forKey: "count") as? Int else {
                        return 0
                    }
                    count += countx
                }
                return count
            }}
        catch {
        }
        return 0
    }
    
    func getCartAmount() -> Double {
        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext
        
        let fetchRequestSender = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        
        fetchRequestSender.returnsObjectsAsFaults = false
        
        do {
            guard let result = try managedContext.fetch(fetchRequestSender) as? [NSManagedObject] else { return 0 }
            
            if result.isEmpty {
                return 0.0
            }
            else {
                
                var price:Double = 0.0
                
                for res in result {
                 
                    let itemPrice = res.value(forKey: "price") as? Double ?? 0.0
                    let count = res.value(forKey: "count") as? Int ?? 0
                    let prod = itemPrice * Double(count)
                    price += prod
                }
                return price
            }}
        catch {
        }
        return 0.0
    }
    
    func clearCart(){

        let managedContext = CoreDataManager.sharedManager.persistantContainer.viewContext

        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Cart")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            try managedContext.execute(request)
            self.delegate?.cartCleared()
        } catch {
            print("could not delete")
        }
    }
}
