//
//  Number+Extension.swift
//  SP
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

extension Int {
    var isZero: Bool {
        return self == 0
    }
}

extension Double {
    var RsString:String {
        return String(format: "Rs %.2f", self)
    }
}
