//
//  UIViewController+Extension.swift
//  SP
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation
import UIKit
import KVNProgress
import SideMenu

extension UIViewController {
    class var storyboardID: String {
        //String(describing: self)
        return "\(self)"
    }
    
    static func instantiate(fromStoryboard storyboard: AppStoryboard) -> Self {
        return storyboard.viewController(viewControllerClass: self) }
    // Shows alert.
    
    func showIndicator(withMessage message: String = "Loading...") {
        hideIndicator()
        KVNProgress.show(withStatus: message)
        
    }
    func showIndicator(withMessage message: String = "Loading...", onView view: UIView? = nil) {
        hideIndicator()
        guard let view = view else {
            KVNProgress.show(withStatus: message)
            return
        }
        KVNProgress.show(withStatus: message, on: view)
    }
    
    func hideIndicator() {
        KVNProgress.dismiss()
    }
    
    
    func hideKeyboardWhenTappedAround() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
    
    func alert(withTitle title: String = "Error",
               desc message: String,
               andActions actions: [UIAlertAction] = [UIAlertAction(title: "OK", style: .default, handler: nil)]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for action in actions {
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }
    
    func addBarItem(addRight: Bool? = false,count: Int? = 0) {
        
        let leftItem: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Group"), style: .plain, target: self, action: #selector(menuBtnTapped))
        leftItem.tintColor = UIColor(hex: "#FFFFFF")
        
        let titleLabel = UILabel()
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold("SMARTWASH")
        titleLabel.attributedText = formattedString
        titleLabel.textColor = UIColor(hex: "#FFFFFF")
        titleLabel.sizeToFit()
        
        let space = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: self, action: nil)
        space.width = 20
        
        let customLeft = UIBarButtonItem(customView: titleLabel)
        
        self.navigationItem.setLeftBarButtonItems([leftItem,space,customLeft], animated: false)
        
        if addRight! {
            
            let cartBtn = UIBadgeButton()
            cartBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
            
            cartBtn.addTarget(self, action: #selector(cartTapped), for: .touchUpInside)
            
            cartBtn.setImage(#imageLiteral(resourceName: "online-shopping-cart(1)v").withRenderingMode(.alwaysTemplate), for: .normal)
            
            cartBtn.badgeEdgeInsets =  UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 7)
            
            cartBtn.badge = String(describing: count!)
            
            cartBtn.tintColor = UIColor(hex: "#FFFFFF")
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: cartBtn)
        }
        else {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        //self.navigationItem.titleView = titleLabel
    }
    
    func addBarWithBackButton(title: String){
        
        let leftItem: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow(1)"), style: .plain, target: self, action: #selector(backTapped))
        leftItem.tintColor = UIColor(hex: "#FFFFFF")
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        space.width = 20
        
        let titleLabel = UILabel()
        let formattedString = NSMutableAttributedString()
        formattedString.normal(title)
        titleLabel.attributedText = formattedString
        titleLabel.textColor = UIColor(hex: "#FFFFFF")
        titleLabel.sizeToFit()
    
        let titleBtn = UIBarButtonItem(customView: titleLabel)
        
        self.navigationItem.setLeftBarButtonItems([leftItem,space,titleBtn], animated: true)
    }
    
    func addCartNavigationBar(showRight: Bool = true){
        
        let leftItem: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "left-arrow(1)"), style: .plain, target: self, action: #selector(backTapped))
        leftItem.tintColor = UIColor(hex: "#FFFFFF")
        
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        space.width = 20
        
        if showRight {
        let rightItem: UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "empty cart").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(clearCart))
            
        self.navigationItem.setRightBarButton(rightItem, animated: true)
        }
        
        let titleLabel = UILabel()
        let formattedString = NSMutableAttributedString()
        formattedString.normal("Cart")
        titleLabel.attributedText = formattedString
        titleLabel.textColor = UIColor(hex: "#FFFFFF")
        titleLabel.sizeToFit()
        
        let titleBtn = UIBarButtonItem(customView: titleLabel)
        
        self.navigationItem.setLeftBarButtonItems([leftItem,space,titleBtn], animated: true)
    }
    
    
    @objc func menuBtnTapped() {
        
        self.present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        
    }
    
    @objc func clearCart() {
        
        let clearAction = UIAlertAction.init(title: "OK", style: .destructive, handler: {
            _ in
            let manager = CoreDataManager.sharedManager
            manager.clearCart()
        })
        
        let noAction = UIAlertAction.init(title: "Cancel", style: .default, handler: nil )
        
    alert(withTitle: "Clear Cart?", desc: "Are you sure you want to clear your cart?", andActions: [noAction,clearAction])
        
    }
    
    @objc func cartTapped() {
        let vc = CartViewController.instantiate(fromStoryboard: .main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makeGradient(onView view: UIView,withColors colors: [CGColor]){
        
        let gradient = CAGradientLayer()
        
        gradient.frame = view.bounds
        gradient.colors = colors
        
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        
        view.layer.insertSublayer(gradient, at: 0)
    }
    
    func sendWhatsappMessage(number: String){
        
        guard number != "" else { return }
        
        guard UIApplication.shared.openURL(URL(string:"https://api.whatsapp.com/send?phone=\(number)")!) else { return }
    }
    
    func callNumber(number: String) {
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}


enum AppStoryboard: String {
    case main = "Main"
    case login = "Login"
    case vehicle = "Vehicle"
    case profile = "Profile"
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = viewControllerClass.storyboardID
        if let vc = instance.instantiateViewController(withIdentifier: storyboardID) as? T {
            return vc
        }
        return T()
    }
}
