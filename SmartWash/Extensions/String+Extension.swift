//
//  String+Extension.swift
//  SP
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

extension String {
    var toURL: URL? {
        return URL(string: self)
    }
    
    var toDouble: Double {
        return Double(self) ?? 0.0
    }
    var isValidName: Bool {
        if self.count >= 2 && count < 50 { return true }
        else { return false }
    }
   
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    var isValidPassword: Bool {
        return self.count > 5
    }
    
    var isValidZIP: Bool {
        return self.count == 6
    }
    
    var isValidPhoneNumber: Bool {
        guard self.count > 9 && self.count < 11 else{
            return false
        }
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
