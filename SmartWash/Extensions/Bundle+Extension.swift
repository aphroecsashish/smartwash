//
//  Bundle+Extension.swift
//  SP
//
//  Created by AphroECS on 24/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import Foundation

extension Bundle {
    static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
        if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
            return view
        }
        fatalError("Could not load view with type " + String(describing: type))
    }
}
