//
//  AddressesViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

enum AddressRequestType: String {
    case delete
    case fetch
    case makeDefault
}

class AddressesViewController: UIViewController {
    
    @IBOutlet weak var noAddressView: UIView!

    var requestType: AddressRequestType? = .fetch
    
    @IBOutlet weak var addressTable: UITableView!
    
    var addresses: [Address] = []
    
    lazy var middleware = APIClient.init(withDelegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addressTable.delegate = self
        self.addressTable.dataSource = self
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        addBarWithBackButton(title: "My Addresses")
        self.showIndicator(withMessage: "Fetching Addresses..")
        self.addresses = []
        self.requestType = AddressRequestType.fetch
        middleware.fetchAddresses()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func backButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func deleteAddress(atIndex: Int) {
        
        self.showIndicator(withMessage: "Deleting Address..")
        
        let addressId = "/\(self.addresses[atIndex].id)"
        
        self.requestType = .delete
        
        middleware.deleteAddress(withParameters: addressId)
    }
    
    @IBAction func addAddressTapped(_ sender: Any) {
       
        let vc = NewAddressViewController.instantiate(fromStoryboard: .profile)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension AddressesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.addresses.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressTableViewCell
        
        let address = self.addresses[indexPath.section]
        
        cell.address.text = address.house + ", " + address.locality + ", "  + address.city + "," + address.zipCode
        
        cell.delegate = self
        cell.editBtn.tag = indexPath.section
        cell.cancelBtn.tag = indexPath.section
        cell.defaultBtn.tag = indexPath.section
        
        let isDefault = address.isDefault
        
        if isDefault == 1 {
            cell.addressView.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            cell.cellTickImage.isHidden = false
            cell.layer.cornerRadius = 5
            cell.addressView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.addressView.layer.borderWidth = 0
            cell.defaultBtn.isHidden = true
        }
        else {
            cell.addressView.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.cellTickImage.isHidden = true
            cell.layer.cornerRadius = 3.5
            cell.addressView.layer.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            cell.addressView.layer.borderWidth = 0.5
            cell.defaultBtn.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension AddressesViewController: AddressTableViewCellDelegate {
    func makeDefaultTapped(atIndex: Int) {
     
        self.requestType = .makeDefault
        
        let addressID = self.addresses[atIndex].id
        
        let params = ["id":addressID] as [String:AnyObject]
        
        showIndicator(withMessage: "making default..")
        
        middleware.updateDefaultAddress(params: params)
    }
    
    
    func editTapped(atIndex: Int) {
        let vc = EditAddressViewController.instantiate(fromStoryboard: .profile)
        vc.address = self.addresses[atIndex]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteTapped(atIndex: Int) {
        self.requestType = AddressRequestType.delete
        self.deleteAddress(atIndex: atIndex)
    }
    
    
}

extension AddressesViewController {
    
    func handleSuccess(object: AnyObject) {
        
        guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
            self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        if self.requestType == AddressRequestType.fetch {
            
            self.addresses = []
            
            let addresses = data["data"] as! [[String:AnyObject]]
            
            if addresses.isEmpty {
                self.noAddressView.isHidden = false
            }
            else {
                self.noAddressView.isHidden = true
            for addr in addresses {
                let adr = Address.init(withJSON: addr)
                self.addresses.append(adr)
            }
                self.addressTable.reloadData()
            }
        }
       
        else if self.requestType == AddressRequestType.makeDefault {
            
            self.addressTable.makeToast("Updated default address")
            
            self.requestType = AddressRequestType.fetch
           
            middleware.fetchAddresses()
        }
            
        else {
            self.showIndicator(withMessage: "Fetching Addresses..")
            self.requestType = AddressRequestType.fetch
            middleware.fetchAddresses()
            self.addressTable.reloadData()
        }
    }
    
    func handleError(object: AnyObject) {
        self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
    }
}

extension AddressesViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            if object != nil {
            self.handleSuccess(object: object!)
            }
        default:
            if object != nil {
            self.handleError(object: object!)
            }
        }
    }
}
