//
//  UpdateProfileViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController, UITextFieldDelegate{

    enum requestType {
        case updateProfile
        case updateNumber
        case updateEmail
    }
    
    var params: [String:AnyObject]?
    
    var currentRequestType: requestType = .updateProfile
    
    @IBOutlet weak var btnResendEmail: UIButton!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    
    
    lazy var middleware = APIClient.init(withDelegate: self)
    var profile: Profile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.name.delegate = self 
        
        //name.keyboardType = UIKeyboardType.alphabet
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.email.text = profile?.email
        self.phoneNumber.text = profile?.mobile
        //name.keyboardType = UIKeyboardType.alphabet
        self.name.text = profile?.name
     
        if self.profile.verified {
         self.btnResendEmail.isHidden = true
        }
        else {
                self.btnResendEmail.isHidden = false
        }
        
        addBarWithBackButton(title: "My Account")
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        
        return allowedCharacters.isSuperset(of: characterSet)
        
        
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendEmailTapped(_ sender: Any) {
        self.currentRequestType = .updateEmail
        
        middleware.resendEmailVerification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        //name.keyboardType = UIKeyboardType.alphabet
        let namex = name.text
        let mobilex = phoneNumber.text
        let emailx = email.text
            
        // showIndicator()
        
        self.params = ["name":namex,"mobile":mobilex,"email":emailx] as [String:AnyObject]
        
        if self.profile?.mobile != mobilex {
            
            guard mobilex!.isValidPhoneNumber else {
                    self.view.makeToast("Invalid mobile Number")
                return
            }
            
            self.currentRequestType = .updateNumber
            showIndicator(withMessage: "Sending OTP")
            let mob = ["mobile":mobilex] as [String:AnyObject]
            
            middleware.sendOTP(withParameters: mob, forMobileChange: true)
        }
        
//        else if self.profile?.email != emailx {
//            //showIndicator(withMessage: "Sending e-mail link")
//            self.currentRequestType = .updateEmail
//        }
//
        else{
        self.currentRequestType = .updateProfile
            showIndicator(withMessage: "Updating profile")
            middleware.updateProfile(params: params ?? [:])
        }
    }
    
    func handleSuccess(object: AnyObject) {
        
        switch currentRequestType {
       
        case .updateEmail:
            self.view.makeToast("Email Verification link re-sent")
            
        case .updateNumber:
        
            let vc = OTPVerificationViewController.instantiate(fromStoryboard: .login)
            
            vc.verificationType = .verifyOTP
            vc.updateData = self.params
            vc.isFromProfile = true
            vc.mobileNumber = self.phoneNumber.text ?? ""
            self.navigationController?.present(vc, animated: true, completion: nil)

        default:
            
            self.view.makeToast("Profile updated successfully.", duration: 3.0)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func handleError(object: AnyObject) {
        
        guard let error = (((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String
            else {
                self.view.makeToast("some error occured, please try again.", duration: 3.0)
                return
        }
        self.view.makeToast(error, duration: 3.0)
    }
}

extension UpdateProfileViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            self.handleSuccess(object: object!)
        default:
            self.handleError(object: object!)
        }
    }
}
