//
//  NewAddressViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import CoreLocation
import SkyFloatingLabelTextField

protocol NewAddressViewControllerDelegate {
    func addressCreated(address: Address)
}

class NewAddressViewController: UIViewController {
    
    var delegate: NewAddressViewControllerDelegate?
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    let picker = UIPickerView()
    var currentLocation: CLLocation?
    var locationManager = CLLocationManager()
    var isStateActive: Bool = true
    var selectedStateInt: Int?
    var city: String?
    var state: String?
    var locality: String?
    var PlacesArray: [Places] = []
    var states: [String] = []
    var cities: [String] = []
    
    var isFromProfile: Bool = true
    
    var localities: [Locality] = []
    var CityID: Int?
    var localityID: Int?
    
    lazy var middleWare:APIClient = APIClient.init(withDelegate: self)
    
    var address: Address?
    var type = addressUpdateType.fetchStates
    
    @IBOutlet weak var houseNo: SkyFloatingLabelTextField!
    @IBOutlet weak var addressText: SkyFloatingLabelTextField!
    @IBOutlet weak var cityText: SkyFloatingLabelTextField!
    @IBOutlet weak var stateText: SkyFloatingLabelTextField!
    @IBOutlet weak var ziptext: SkyFloatingLabelTextField!
    
    enum addressUpdateType: String {
        case new
        case fetchStates
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cityText.isUserInteractionEnabled = false
        
        self.cityText.inputView = picker
        self.stateText.inputView = picker
        picker.dataSource = self
        picker.delegate = self
        
        self.stateText.delegate = self
        self.cityText.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addBarWithBackButton(title: "New Address")
        showIndicator()
        middleWare.fetchCityAndLocality()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAddressTapped(_ sender: Any) {
     
        self.type = addressUpdateType.new
        
        guard let address = houseNo.text, address != "" else {
           
            self.view.makeToast("Please enter a correct address", duration: 3.0, position: .bottom)
            return }
        
        guard let addressOne = addressText.text, address != "" else {
             self.view.makeToast("Please enter a correct address", duration: 3.0, position: .bottom)
            return }
        
        guard let zipCode = ziptext.text, zipCode.isValidZIP else{
             self.view.makeToast("Invalid zip", duration: 3.0, position: .bottom)
            return }
        
        guard let _ = state, let _ = city else {
 self.view.makeToast("Please enter all the fields", duration: 3.0, position: .bottom)
            return
        }
        
        guard CityID != nil, localityID != nil else {
             self.view.makeToast("Please select state and city", duration: 3.0, position: .bottom)
            return
        }
        
        let param = ["city": self.CityID!,"state": self.PlacesArray[selectedStateInt!].stateID ,"house":address,"locality":self.localityID!,"zip_code":zipCode,"address_one":addressOne] as [String:AnyObject]
        
        showIndicator(withMessage: "Creating Address")
        middleWare.createAddress(params: param)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension NewAddressViewController: UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource {
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == stateText {
            
            if self.selectedStateInt != nil {
                self.cityText.isUserInteractionEnabled = true
            }
            else {
                 self.view.makeToast("Please select a city", duration: 3.0, position: .bottom)
            }
            
        }
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == stateText {
            self.cities = []
            self.cityText.isUserInteractionEnabled = false
            self.cityText.text = ""
            isStateActive = true
            picker.reloadAllComponents()
            cityText.resignFirstResponder()
            self.becomeFirstResponder()
        }
        if textField == cityText {
            picker.reloadAllComponents()
            stateText.resignFirstResponder()
            self.becomeFirstResponder()
                isStateActive = false
                
                let citix = self.PlacesArray[selectedStateInt!]
                
                self.cities = []
                
                for city in citix.localities {
                    
                    self.cities.append(city.name)
                }
        }}
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if isStateActive {
            return PlacesArray.count
        }
        else {
            return self.cities.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if isStateActive {
                self.selectedStateInt = row
                self.state = self.PlacesArray[row].name
                self.stateText.text = self.PlacesArray[row].name
                self.CityID = self.PlacesArray[row].id
                self.localities = self.PlacesArray[row].localities
            
                let cities = self.PlacesArray[selectedStateInt!]
            
                self.cities = []
            
                for city in cities.localities {
                
                self.cities.append(city.name)
                }
        }
        else {
            self.cityText.text = self.cities[row]
            self.city = self.cities[row]
            self.localityID = self.localities[row].id
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if isStateActive {
            return self.PlacesArray[row].name
        }
        else {
            return self.cities[(row > cities.count - 1) ? 0 : row]
        }
    }
}


extension NewAddressViewController {
    
    func handleSuccess(object: AnyObject) {
        
        if self.type == .fetchStates {
        
        guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
             self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        let places = data["data"] as? [[String:AnyObject]]
        
        for placex in places! {
        
        let place = Places.init(withJSON: placex)
        
        self.PlacesArray.append(place)
            }}
        
        else if self.type == .new {

            guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
            }
            
            let newAddress =  Address.init(withJSON: data["data"] as! [String:AnyObject])
            
            self.delegate?.addressCreated(address: newAddress)
            
            self.navigationController?.popViewController(animated: true)
            
//            self.navigationController?.dismiss(animated: true, completion: {
//
//            })
        }
    }
    
    func handleError(object: AnyObject) {
        self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
    }
}

extension NewAddressViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            self.handleSuccess(object: object!)
        default:
            self.handleError(object: object!)
        }
    }
}
