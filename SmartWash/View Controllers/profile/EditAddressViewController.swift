//
//  EditAddressViewController.swift
//  
//
//  Created by AphroECS on 27/09/18.
//

import UIKit
import SkyFloatingLabelTextField

enum requestType{
    case fetchCities
    case updateAddress
}

class EditAddressViewController: UIViewController {

    lazy var middleware = APIClient.init(withDelegate: self)
    
    var address: Address!
    
    var reqType: requestType = .fetchCities
    
    @IBOutlet weak var addressOne: SkyFloatingLabelTextField!
    var PlacesArray: [Places] = []
    var localities: [Locality] = []
    
    var isStateActive = false
    var cityActive: Bool = true
    var cityArray: [String] = []
    var localityArray: [String] = []
    var selectedStateInt: Int? = nil
    
    var localityID: Int?
    var cityID: Int?
    
    @IBOutlet weak var houseNo: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfCity: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfZip: SkyFloatingLabelTextField!
    
    @IBOutlet weak var tfLocality: SkyFloatingLabelTextField!
    
    var picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        self.tfLocality.delegate = self
        self.tfCity.delegate = self
        
        picker.dataSource = self
        picker.delegate = self
        
        tfCity.inputView = picker
        tfLocality.inputView = picker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        addBarWithBackButton(title: "Edit Address")
        
        showIndicator()
        middleware.fetchCityAndLocality()
        
        self.houseNo.text = self.address.house
        self.tfCity.text = self.address.city
        self.tfLocality.text = self.address.locality
        self.tfZip.text = self.address.zipCode
        self.addressOne.text = self.address.addressOne
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        
        self.reqType = .updateAddress
        
        guard let hn = self.houseNo.text, let _ = self.tfCity.text, let _ = tfLocality.text, let zip = tfZip.text, let addressOne = addressOne.text else {
            self.view.makeToast("Please enter all the fields", duration: 3.0, position: .bottom)
            return
        }
        
        guard cityID != nil , localityID != nil else {
            
            self.view.makeToast("Please select city and Locality", duration: 3.0,position: .bottom)
        
                                
            return }
        
        let params = ["house":hn,"address_one": addressOne, "city":cityID!,"locality":localityID!,"zip_code":zip,"id":self.address.id] as [String : AnyObject]
        showIndicator()
        middleware.updateAddress(params: params)
    }
}

extension EditAddressViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfCity {
            isStateActive = true
            self.tfLocality.isUserInteractionEnabled = false
            self.tfLocality.text = ""
        }
        
        if textField == tfLocality {
            
            if self.selectedStateInt == nil {
                self.view.makeToast("Please select a State First", duration: 3.0,position: .bottom)
            }
            else {
                isStateActive = false
                
                let cities = self.PlacesArray[selectedStateInt!]
                
                for city in cities.localities {
                    
                    self.localityArray.append(city.name)
                }
            }
        }}
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfCity {
            if self.selectedStateInt == nil {
                self.view.makeToast("Please select a City", duration: 3.0,position: .bottom)
            }
            else {
                tfLocality.isUserInteractionEnabled = true
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if isStateActive {
            return PlacesArray.count
        }
        else {
            return localityArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if isStateActive {
            self.selectedStateInt = row
            self.tfLocality.isUserInteractionEnabled = true
            self.tfCity.text = self.PlacesArray[row].name
            self.cityID = self.PlacesArray[row].id
            self.localities = PlacesArray[row].localities
            self.tfLocality.text = ""
        }
        else {
            self.tfLocality.text = self.localityArray[row]
            self.localityID = localities[row].id
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if isStateActive {3
            return self.PlacesArray[row].name
        }
        else {
            return self.localityArray[row]
        }
    }
    
}

extension EditAddressViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        hideIndicator()
        switch status {
            
        case .success:
        if  self.reqType == .fetchCities {
                
                guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
                    self.view.makeToast("Some error occured", duration: 3.0)
                    return
                }
            
                self.PlacesArray = []
            
                let places = data["data"] as? [[String:AnyObject]]
                
                for placex in places! {
                    
                    let place = Places.init(withJSON: placex)
                    
                    self.PlacesArray.append(place)
                }
        }
        else {
            self.navigationController?.popViewController(animated: true)
            }
            
        default: print("something else.")
        
        }
        
    }
}
