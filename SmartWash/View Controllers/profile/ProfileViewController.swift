//
//  ProfileViewController.swift
//  SmartWash
//
//  Created by AphroECS on 12/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import SideMenu
import Toast_Swift

class ProfileViewController: UIViewController {

    var profile: Profile?
    
    lazy var middleware = APIClient.init(withDelegate: self)
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchProfile()
        addBarWithBackButton(title: "My Account")
    }
    
    @IBAction func changePasswordTapped(_ sender: Any) {}
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func manageAddressTapped(_ sender: Any) {
        
        let vc = AddressesViewController.instantiate(fromStoryboard: .profile)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func EditTapped(_ sender: Any) {
        
        let vc = UpdateProfileViewController.instantiate(fromStoryboard: .profile)
        vc.profile = self.profile
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func fetchProfile() {
        
        guard let token = UserDefaults.standard.value(forKey: "token") as? String
            else {
                return  
        }
        self.showIndicator()
        middleware.fetchProfile(token: "Bearer "+token)
    }
    
    func handleSuccess(object: AnyObject) {
        
        print(object)
        
        guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
            self.view.makeToast("Some error occured", duration: 3.0)
            return
        }
        
        let response = Response.init(withData: data)
        
        self.profile = response.profile
        
        UserDefaults.standard.setValue(profile?.name, forKey: "name")
        
        self.nameLabel.text = profile?.name
        self.emailLabel.text = profile?.email
        self.phoneLabel.text = profile?.mobile
    }
    
    func handleError(object: AnyObject) {
        
        print(object)
        
    }
}

extension ProfileViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            
            self.handleSuccess(object: object!)
            
        default:
            
            self.handleError(object: object!)
            
        }
    }
}

