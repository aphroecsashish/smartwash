//
//  EnterMobileNumberViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class EnterMobileNumberViewController: UIViewController {

    lazy var middleWare: APIClient = APIClient.init(withDelegate: self)

    var mobile: String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBOutlet weak var mobileNumberField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        
        dismissKeyboard()
        
        guard let mobile = mobileNumberField.text, mobile.isValidPhoneNumber else {
            self.view.makeToast("Invalid mobile number", duration: 3.0, position: .bottom)
            return }
        
        self.mobile = mobile
        let param = ["mobile":mobile] as [String:AnyObject]
        showIndicator(withMessage: "Sending OTP")
        middleWare.sendOTP(withParameters: param)
        
    }
    
    func handleSuccess(object: AnyObject) {
        
        let vc = OTPVerificationViewController.instantiate(fromStoryboard: .login)
        
        vc.verificationType = OTPVerificationViewController.otpType(rawValue: "verifyOTP")
        vc.currentRequest = .change
        vc.mobileNumber = self.mobile
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleError(object: AnyObject) {
        
        guard let error = (((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String
            else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
        }
        self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
}

extension EnterMobileNumberViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            
            self.handleSuccess(object: object!)
            
        default:
            
            self.handleError(object: object!)
            
        }
    }
}
