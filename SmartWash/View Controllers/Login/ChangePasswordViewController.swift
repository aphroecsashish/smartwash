//
//  ChangePasswordViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    enum requestType {
        case newPassword
        case changePassword
    }
    
    var currentRequest: requestType = .newPassword
    
    lazy var middleWare: APIClient = APIClient.init(withDelegate: self)
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        if currentRequest == .newPassword {
            lblTitle.text = "Create Password"
        }
        else {
            lblTitle.text = "Change Password"
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        
        dismissKeyboard()
        
        guard let password = newPassword.text, let confirmPass = confirmPassword.text, password == confirmPass
            
            else {
            self.view.makeToast("Passwords don't match", duration: 3.0, position: .bottom)
            return
        }
        
        if password.isValidPassword {
            let params = ["password":password] as [String:AnyObject]
            
            showIndicator(withMessage: "Changing Password")
            middleWare.changePassword(params: params)
        
        }
        else {
            self.view.makeToast("Password should be atleast 6 Characters.", duration: 3.0, position: .bottom)
            return
        }
        
    }
    
    @IBAction func togglePassword(_ sender: UIButton) {
        
        self.confirmPassword.isSecureTextEntry = !self.confirmPassword.isSecureTextEntry
        
        self.newPassword.isSecureTextEntry = !self.newPassword.isSecureTextEntry
        
        self.oldPassword.isSecureTextEntry = !self.oldPassword.isSecureTextEntry
    }
    
    func handleSuccess(object: AnyObject) {
        
        let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: { _ in
           
            let vc = HomeViewController.instantiate(fromStoryboard: .main)
            UserDefaults.standard.set(true,forKey: Keys.login)
            self.navigationController?.pushViewController(vc, animated: true)
        })
        
        if self.currentRequest == .newPassword {
            self.alert(withTitle: "Password Created", desc: "Your password has been created.", andActions: [okAction])
        }
        else {
            self.alert(withTitle: "Password Changed", desc: "Your password has been changed.", andActions: [okAction]) }
    }
    
    func handleError(object: AnyObject) {
        
        guard let error = (((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String
            else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
        }
        self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
}


extension ChangePasswordViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            
            self.handleSuccess(object: object!)
            
        default:
            
            self.handleError(object: object!)
            
        }
    }
}
