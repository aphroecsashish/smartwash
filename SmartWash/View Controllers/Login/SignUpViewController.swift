//
//  SignUpViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    var mobile = ""
    
    lazy var middleware = APIClient.init(withDelegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func togglePassword(_ sender: UIButton) {
        self.password.isSecureTextEntry = !self.password.isSecureTextEntry
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func signUpTapped(_ sender: UIButton) {
    
        dismissKeyboard()
        
        guard let name = fullName.text, name.isValidName else {
            self.view.makeToast("Invalid name", duration: 3.0, position: .bottom)
            return
        }
        guard let mobile = mobileNumber.text,mobile.isValidPhoneNumber else {
            self.view.makeToast("Invalid mobile number", duration: 3.0, position: .bottom)
            return
        }
        
        if email != nil && email.text!.count > 0 {
            guard email.text!.isValidEmail else {
                self.view.makeToast("Invalid email", duration: 3.0, position: .bottom)
                return
            }
        }
        
        showIndicator()
        
        let params = ["mobile":mobile,"name":name,"email":email.text ?? ""]
        
        self.mobile = mobile
        
        middleware.signup(withParameters: params as [String : AnyObject])
        
    }
    
    func handleSuccess(_ object: AnyObject) {
        
        let vc = OTPVerificationViewController.instantiate(fromStoryboard: .login)
        
        vc.verificationType = OTPVerificationViewController.otpType(rawValue: "verifyOTP")
        vc.currentRequest = .new
        vc.mobileNumber = self.mobile
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleError(object: AnyObject) {
        
        guard let error = (((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String
            else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
        }
        self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
}

extension SignUpViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            
            handleSuccess(object!)
            
        default:
            
            handleError(object: object!)
            
        }
    }
}
