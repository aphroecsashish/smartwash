//
//  MainViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
    }
    
    @IBAction func loginTapped(_ sender: Any) {
    
    }
}
