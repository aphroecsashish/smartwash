//
//  LoginViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var phoneNumber: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    lazy var middleware = APIClient.init(withDelegate: self)
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func toggleShowPassword(_ sender: Any) {
        self.password.isSecureTextEntry = !self.password.isSecureTextEntry
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        dismissKeyboard()
        
        guard  let userID = phoneNumber.text, userID.isValidPhoneNumber else {
            self.view.makeToast("Invalid ID", duration: 3.0, position: .bottom)
            return
        }
    
        guard  let password = password.text, password.isValidPassword else {
            self.view.makeToast("Invalid password", duration: 3.0, position: .bottom)
            return
        }
        
        let params = ["mobile":userID, "password": password] as [String:AnyObject]
        
        showIndicator(withMessage: "Logging in..")
        
        middleware.login(withParameters: params)
    }
    
    @IBAction func forgotPasswordTapped(_ sender: Any) {
        
        let mobileNumberVC = EnterMobileNumberViewController.instantiate(fromStoryboard: .login)
        
        self.navigationController?.pushViewController(mobileNumberVC, animated: true)
    }
    
    func handleSuccess(object: AnyObject) {
        
        print(object)
        
        guard let data = (((object as! NSDictionary).value(forKey: "response") as? NSDictionary)?.value(forKey: "data") as? NSDictionary) else {
            self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        guard let token = data.value(forKey: "token") as? String, let userID = data.value(forKey: "userID") else {
            self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        guard let name = data.value(forKey: "name") as? String else {return}
        
        UserDefaults.standard.set(token, forKey: "token")
        UserDefaults.standard.set(userID, forKey: "userID")
        UserDefaults.standard.set(name, forKey: "name")
        UserDefaults.standard.set(true, forKey: Keys.login)
        
        let homeVC = HomeViewController.instantiate(fromStoryboard: .main)
        
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func handleError(object: AnyObject) {
        
        guard let error = (((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String
            else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
        }
        
        self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
}

extension LoginViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            
            self.handleSuccess(object: object!)
            
        default:
        
            self.handleError(object: object!)
            
        }
    }
}
