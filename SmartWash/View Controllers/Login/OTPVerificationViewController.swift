//
//  OTPVerificationViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import Toast_Swift

class OTPVerificationViewController: UIViewController {
    
    var isFromProfile: Bool = false
    var updateData: [String:AnyObject]?
    
    enum chngPassReqType {
        case new
        case change
    }
    
    var currentRequest: chngPassReqType = .new
    
    enum otpType:String {
        case verifyOTP
        case resendOTP
        case updateProfile
    }
    
    var mobileNumber:String = ""
    var verificationType: otpType?
    
    lazy var middleware = APIClient.init(withDelegate: self)
    
    @IBOutlet weak var alreadRegisteredView: UIStackView!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField1: UITextField!
    
    var otp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        textField1.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.isFromProfile {
            self.alreadRegisteredView.isHidden = true
        }
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        if self.isFromProfile {
            self.dismiss(animated: true, completion: nil)
        }
        else {
        self.navigationController?.popViewController(animated: true)
        }}
    
    @IBAction func goToLogin(_ sender: Any) {
        DefaultProperties.appDelegate.showLogin()
    }
    
    @IBAction func confirmOTPTapped(_ sender: UIButton) {

        dismissKeyboard()
        
        self.verificationType = otpType.verifyOTP
        
        if otp.count == 4 {
        
        let params = ["mobile": mobileNumber,"otp": otp] as [String:AnyObject]
        showIndicator(withMessage: "Verifying OTP.")
            if self.isFromProfile {
                middleware.verifyOTP(withParameters: params, forMobileChange: true)
            }
            else {
        middleware.verifyOTP(withParameters: params)
            }}
        else {
        
        }
    }
    
    @IBAction func resentOTPTapped(_ sender: UIButton) {
        dismissKeyboard()
        self.verificationType = otpType.resendOTP
        
        if self.isFromProfile {}
        else {
        sendOTP(mobile: self.mobileNumber)
        }}
    
    @IBAction func goToLoginTapped(_ sender: UIButton) {
        
    }
    
    func sendOTP(mobile: String) {
        
        if self.isFromProfile {
            middleware.sendOTP(withParameters: ["mobile":mobile as AnyObject], forMobileChange: true)
        }
        else {
        middleware.sendOTP(withParameters: ["mobile":mobile as AnyObject])
        }}
    
    func handleResponse(_ object: AnyObject) {
        
        if self.verificationType == otpType.resendOTP {
            
            self.view.makeToast("OTP resent", duration: 3.0, position: .bottom)
        }
            
        else if self.verificationType == otpType.updateProfile {
         
//            let vc = ProfileViewController.instantiate(fromStoryboard: .profile)
//
//            self.dismiss
            
            self.dismiss(animated: true, completion: {
            })
            
        }
            
        else {
            
            guard let data = (object as? NSDictionary)?.value(forKey: "data") as? NSDictionary else {
                
                if let data = object as? [String:AnyObject], let status = data["status"] as? String {
                    
                    let msg = data["message"] as? String ?? "Some error occured"
                    self.view.makeToast("msg")

                    if status == "success" {
                        
                        if self.isFromProfile {
                            
                            showIndicator(withMessage: "updating profile")
                            
                            self.verificationType == otpType.updateProfile
                            
                            middleware.updateProfile(params: self.updateData!)
                            
                        }
                        
                    } else {
                    }
                }
                else {
                    self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                    return
                }
                return
            }
            
            guard let token = data.value(forKey: "token") as? String, let userID = data.value(forKey: "userID") else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
            }
            
            UserDefaults.standard.set(token, forKey: "token")
            UserDefaults.standard.set(userID, forKey: "userID")
            
            if self.isFromProfile {
                self.dismiss(animated: true, completion: {
                })
            }
            else {
            
            let vc = ChangePasswordViewController.instantiate(fromStoryboard: .login)
                
                if self.currentRequest == .new {
                    vc.currentRequest = .newPassword
                }
                else {
                    vc.currentRequest = .changePassword }
            self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func handleError(object: AnyObject) {
        
        guard let error = ((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "message") as? String
            else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
        }
        self.view.makeToast(error, duration: 3.0, position: .bottom)
        resetTextField(textFields: [textField1,textField2,textField3,textField4])
    }
    
}

extension OTPVerificationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textField1 {
            resetTextField(textFields: [textField2,textField3,textField4])
        }
        
        if textField == textField2 {
            resetTextField(textFields: [textField3,textField4])
        }
        
        if textField == textField3 {
            resetTextField(textFields: [textField4])
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let char = string.cString(using: String.Encoding.utf8)
        let backspace = strcmp(char,"\\b")
        
        if(backspace == -92) {
            
            if textField == textField1 {
                resetTextField(textFields: [textField1,textField2,textField3,textField4])
                
            }
            
            if textField == textField2 {
                resetTextField(textFields: [textField2,textField3,textField4])
                
                if string.count == 0 {
                    textField1.becomeFirstResponder()
                }
                
            }
            
            if textField == textField3 {
                resetTextField(textFields: [textField3,textField4])
                
                if string.count == 0 {
                    textField2.becomeFirstResponder()
                }
            }
            
            if textField == textField4 {
                resetTextField(textFields: [textField4])
                
                if string.count == 0 {
                    textField1.becomeFirstResponder()
                }
                
            }
            return true
        }
        
        if string.count == 1 {
            
            textField.text = string
            
            otp.append(string)
            
            if textField == textField1 {
                textField2.becomeFirstResponder()
            }
            if textField == textField2 {
                textField3.becomeFirstResponder()
            }
            if textField == textField3 {
                textField4.becomeFirstResponder()
            }
            if textField == textField4 {
                textField4.resignFirstResponder()
            }
            return true
        }
        else {
            return false
        }
    }
    
    func resetTextField(textFields: [UITextField]){
        
        for textfield in textFields {
            textfield.text = ""
        }
    if textFields.count == 4 || textFields.count == 3 {
    self.otp = ""
        }}
}

extension OTPVerificationViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            handleResponse(object!)
        default:
            handleError(object: object!)
        }
    }
}
