//
//  ViewController.swift
//  SmartWash
//
//  Created by AphroECS on 29/08/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage

class HomeViewController: UIViewController {
    
    var manager = CoreDataManager.sharedManager
    
    var selectedServiceType: ServiceType = .washIron
    
    lazy var middleWare: APIClient = APIClient(withDelegate: self)
  
    var catList: [Category] = []
    
    var ironCatList: [Category] = []
    var washCatList: [Category] = []
    var DryCatList: [Category] = []
    var petrolCatList: [Category] = []
    
    @IBOutlet weak var continueCheckoutBtn: UIView!
    @IBOutlet weak var userOptionsView: UIStackView!
    @IBOutlet weak var homeTableView: UITableView!
    
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var numberOfItems: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
   
    @IBOutlet weak var imgWashNIron: UIImageView!
    @IBOutlet weak var titleWashnDry: UILabel!
    
    @IBOutlet weak var imgDryClean: UIImageView!
    @IBOutlet weak var titleDryClean: UILabel!
    
    @IBOutlet weak var imgPetrol: UIImageView!
    @IBOutlet weak var titleIroning: UILabel!
    
    @IBOutlet weak var titlePetrolWash: UILabel!
    @IBOutlet weak var imgIron: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        showIndicator()
        middleWare.fetchCategories()
        homeTableView.register(HeaderTableViewCell.self)
        homeTableView.register(ContainerTableViewCell.self)
        homeTableView.register(ListTableViewCell.self)
       // homeTableView.sectionHeaderHeight = 60
        imgWashNIron.image = #imageLiteral(resourceName: "washNIronSelected")
        setupSideMenu()
    }
    
    func setupSideMenu() {
        SideMenuManager.default.menuLeftNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: (self.navigationController?.view)!)
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
       // addBarItem(addRight: false)
        
        manager.delegate = self
        getCount()
        
        self.homeTableView.reloadData()
        
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getCount() {
        
        let itemsInCart = manager.getCartCount()
        
        if itemsInCart == 0 {
            addBarItem(addRight: false)
            self.userOptionsView.isHidden = false
            self.continueCheckoutBtn.isHidden = true
        }
            
        else {
            addBarItem(addRight: true,count: itemsInCart)
        
            self.userOptionsView.isHidden = true
            self.continueCheckoutBtn.isHidden = false
            self.numberOfItems.text = String(describing: itemsInCart) + " Items"
            let price = manager.getCartAmount()
            self.totalPrice.text = price.RsString
        }
    }
    
    @IBAction func callustapped(_ sender: Any) {
        
        let vc = PopUpViewController.instantiate(fromStoryboard: .main)
        vc.modalPresentationStyle = .overCurrentContext
        vc.currentRequest = .contact
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func expressTapped(_ sender: Any) {
        
        let vc = PopUpViewController.instantiate(fromStoryboard: .main)
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        vc.currentRequest = .alert
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func continueCheckoutTapped(_ sender: Any) {
        let vc = CartViewController.instantiate(fromStoryboard: .main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func washAndIrontapped(_ sender: Any) {
        self.selectedServiceType = .washIron
        
            imgIron.image = #imageLiteral(resourceName: "iron")
            imgPetrol.image = #imageLiteral(resourceName: "petrol")
            imgDryClean.image = #imageLiteral(resourceName: "dryClean")
            imgWashNIron.image = #imageLiteral(resourceName: "washNIronSelected")
        
            titleWashnDry.textColor = #colorLiteral(red: 0.2745098039, green: 0.4352941176, blue: 0.7529411765, alpha: 1)
            titleDryClean.textColor = UIColor.darkGray
            titleIroning.textColor = UIColor.darkGray
            titlePetrolWash.textColor = UIColor.darkGray

        self.homeTableView.reloadData()
    }
    
    @IBAction func dryCleanTapped(_ sender: Any) {
        self.selectedServiceType = .dryClean
        
        imgIron.image = #imageLiteral(resourceName: "iron")
        imgPetrol.image = #imageLiteral(resourceName: "petrol")
        imgDryClean.image = #imageLiteral(resourceName: "dryCleanSelected")
        imgWashNIron.image = #imageLiteral(resourceName: "washNiron")
        
        titleWashnDry.textColor = UIColor.darkGray
        titleDryClean.textColor = #colorLiteral(red: 0.2745098039, green: 0.4352941176, blue: 0.7529411765, alpha: 1)
        titleIroning.textColor = UIColor.darkGray
        titlePetrolWash.textColor = UIColor.darkGray
        
        self.homeTableView.reloadData()
    }
    
    @IBAction func petrolWashTapped(_ sender: Any) {
        self.selectedServiceType = .petrolWash
        
        imgIron.image = #imageLiteral(resourceName: "iron")
        imgPetrol.image = #imageLiteral(resourceName: "petrolSelected")
        imgDryClean.image = #imageLiteral(resourceName: "dryClean")
        imgWashNIron.image = #imageLiteral(resourceName: "washNiron")
        
        titleWashnDry.textColor = UIColor.darkGray
        titleDryClean.textColor =  UIColor.darkGray
        titleIroning.textColor = UIColor.darkGray
        titlePetrolWash.textColor = #colorLiteral(red: 0.2745098039, green: 0.4352941176, blue: 0.7529411765, alpha: 1)
        
        self.homeTableView.reloadData()
    }
    
    @IBAction func ironingTapped(_ sender: Any) {
        self.selectedServiceType = .iron
        
        imgIron.image = #imageLiteral(resourceName: "ironSelected")
        imgPetrol.image = #imageLiteral(resourceName: "petrol")
        imgDryClean.image = #imageLiteral(resourceName: "dryClean")
        imgWashNIron.image = #imageLiteral(resourceName: "washNiron")
        
        titleWashnDry.textColor = UIColor.darkGray
        titleDryClean.textColor =  UIColor.darkGray
        titleIroning.textColor = #colorLiteral(red: 0.2901960784, green: 0.4588235294, blue: 0.7843137255, alpha: 1)
        titlePetrolWash.textColor = UIColor.darkGray
        
        self.homeTableView.reloadData()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.selectedServiceType == .washIron {
            return self.washCatList.count
        }
            
        else if self.selectedServiceType == .iron {
            return self.ironCatList.count
        }
        else if self.selectedServiceType == .dryClean {
            return self.DryCatList.count
        }
        else if self.selectedServiceType == .petrolWash {
            return self.petrolCatList.count
        }
        else {
        return 0 //self.catList.count
        }}
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var cat: Category?
        
        if self.selectedServiceType == .washIron {
            cat =  washCatList[indexPath.section]
        }
        else if self.selectedServiceType == .iron {
            cat =  ironCatList[indexPath.section]
        }
        else if self.selectedServiceType == .dryClean {
            cat =  DryCatList[indexPath.section]
        }
        else if self.selectedServiceType == .petrolWash {
            cat =  petrolCatList[indexPath.section]
        }
        
        var height = 0
        if cat!.expanded {
            let subCats = cat!.subCategories
            height += cat!.subCategories.count * 70
            for subCat in subCats {
                if subCat.expanded {
                    
                    var isZero: Bool = false
                    
                    var pCount = 0
                    
                    for prod in subCat.products {
                        
                        for pri in prod.productPrices{
                    
                            if pri.serviceType == self.selectedServiceType && pri.price != 0.0 {
                                height += 70
//                                isZero = true
//                                pCount += 1
//                                break
                            }
                        }
                    }
                  //  isZero ? (height += 0) : (height += (subCat.products.count - pCount) * 70)
                }
            }
        }
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var cat2: [Category] = []
        
        if self.selectedServiceType == .washIron {
            cat2 =  self.washCatList
        }
        else if self.selectedServiceType == .iron {
            cat2 =  self.ironCatList
        }
        else if self.selectedServiceType == .dryClean {
            cat2 =  self.DryCatList
        }
        else if self.selectedServiceType == .petrolWash {
            cat2 =  self.petrolCatList
        }
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as? HeaderTableViewCell {
            
            cell.headerTitle.text = cat2[section].name
            
            cell.cellImage.sd_setImage(with: cat2[section].image.toURL, placeholderImage: #imageLiteral(resourceName: "user(7)") , options: [.progressiveDownload,.continueInBackground], completed: nil)
            cell.expandBtn.tag = section
            cell.delegate = self

            return cell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.selectedServiceType == .washIron {
            return self.washCatList[section].expanded ? 1 : 0
        }
        else if self.selectedServiceType == .iron {
            return self.ironCatList[section].expanded ? 1 : 0
        }
        else if self.selectedServiceType == .dryClean {
            return self.DryCatList[section].expanded ? 1 : 0
        }
        else if self.selectedServiceType == .petrolWash {
            return self.petrolCatList[section].expanded ? 1 : 0
        }
        else {
            return 0 //self.catList.count
        }
        
        //return catList[section].expanded ? 1 : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cat2: [Category] = []
        
        if self.selectedServiceType == .washIron {
            cat2 =  self.washCatList
        }
        else if self.selectedServiceType == .iron {
            cat2 =  self.ironCatList
        }
        else if self.selectedServiceType == .dryClean {
            cat2 =  self.DryCatList
        }
        else if self.selectedServiceType == .petrolWash {
            cat2 =  self.petrolCatList
        }

        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContainerTableViewCell")  as? ContainerTableViewCell {
            
            cell.category = cat2[indexPath.section]
            cell.containerSuperIndex = indexPath
            cell.delegate = self
            cell.selectedServiceType = self.selectedServiceType
            cell.tableView.reloadData()
            return cell
        }
        return UITableViewCell()
    }
}

extension HomeViewController: SubHeaderTableViewCellDelegate {
   
    func expandTapped(atIndex index: Int) {
        
        if self.selectedServiceType == .washIron {
            self.washCatList[index].expanded = !self.washCatList[index].expanded
        }
        else if self.selectedServiceType == .iron {
           self.ironCatList[index].expanded = !self.ironCatList[index].expanded
        }
        else if self.selectedServiceType == .dryClean {
            self.DryCatList[index].expanded = !self.DryCatList[index].expanded
        }
        else if self.selectedServiceType == .petrolWash {
            self.petrolCatList[index].expanded = !self.petrolCatList[index].expanded
        }
       // catList[index].expanded = !catList[index].expanded
        self.homeTableView.reloadData()
    }
}

extension HomeViewController: ContainerTableViewCellDelegate {
    func expand() {
        homeTableView.reloadData()
    }
}

extension HomeViewController: HTTPDelegate {
    
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        hideIndicator()
        
        switch status {
        case .success:
            handleResponse(withObject: object!)
        default:
            handleError(withObject: object ?? "" as AnyObject)
        }
    }
    
    func handleResponse(withObject object: AnyObject?) {
        
        guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
            self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        guard let categories = (data["data"] as? NSDictionary)?.value(forKey: "categories") as? [[String:AnyObject]] else {
           self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        var categorie: [Category] = []
        for cat in categories {
            let category = Category.init(withJSON: cat)
            category.expanded = true
            categorie.append(category)
        }
        self.catList =  categorie
        
        self.filterData()
        
        self.homeTableView.reloadData()
    }
    
    func filterData() {
        
        for category in self.catList {
            
            for subCat in category.subCategories {
                
                for prod in subCat.products {
                    
                    if prod.isPetrolAvailable! {
                        if !petrolCatList.contains(where: {
                            $0.id == category.id
                        }) {
                                self.petrolCatList.append(category)
                        }}
                    
                    if prod.isWashAvailable! {
                        if !washCatList.contains(where: {
                            $0.id == category.id
                        }) {
                            self.washCatList.append(category)
                        }}
                    
                    if prod.isIronAvailable! {
                        if !ironCatList.contains(where: {
                            $0.id == category.id
                        }) {
                            self.ironCatList.append(category)
                        }
                    }
                    
                    if prod.isDryAvailable! {
                        if !DryCatList.contains(where: {
                            $0.id == category.id
                        }) {
                            self.DryCatList.append(category)
                        }
                    }
                    
                }
                continue
            }
        }
    }
    
    func handleError(withObject object: AnyObject?) {}
    
}


extension HomeViewController: CoreDataManagerDelegate {
    func cartCleared() {
    }
    
    func cartUpdated(withProduct: Product) {
        self.getCount()
//        self.homeTableView.reloadData()
    }
}

extension HomeViewController : PopUpViewControllerDelegate {
    func shouldGoToSchedulePickup() {
        
        let vc = SchedulePickupViewController.instantiate(fromStoryboard: .main)
        vc.pickup = .express
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
