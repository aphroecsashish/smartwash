//
//  PopUpViewController.swift
//  SmartWash
//
//  Created by AphroECS on 03/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

protocol PopUpViewControllerDelegate {
    func shouldGoToSchedulePickup()
}

class PopUpViewController: UIViewController {

    var delegate : PopUpViewControllerDelegate?
    
    @IBOutlet weak var callOptionsStack: UIStackView!
    @IBOutlet weak var requestPickupAlertView: UIView!
    
    enum requestType {
        case alert
        case contact
    }
    
    var currentRequest: requestType = .contact
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.currentRequest == .alert {
            self.callOptionsStack.isHidden = true
            self.requestPickupAlertView.isHidden = false
        }
        else {
            self.callOptionsStack.isHidden = false
            self.requestPickupAlertView.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callTapped(_ sender: UIButton) {
        callNumber(number: sender.titleLabel?.text ?? "")
    }
    
    @IBAction func yesTapped(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.shouldGoToSchedulePickup()
        }
    }
    
    @IBAction func whatsappTapped(_ sender: UIButton) {
        sendWhatsappMessage(number: sender.titleLabel?.text ?? "")
    }
    
}
