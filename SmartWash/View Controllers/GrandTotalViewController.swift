//
//  ViewController.swift
//  IQKeyboardManager
//
//  Created by AphroECS on 18/10/18.
//

import UIKit

class GrandTotalViewController: UIViewController {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var lblTitleGrandTotal: UILabel!
    
    var orderDetails: [String:AnyObject] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.title = "Thank you"
        
        guard let data = self.orderDetails["data"] as? [String: AnyObject] else { return }
        
        if let address = data["user_locations"] as? [String:AnyObject] {
            
            let house = (address["house"] as? String ?? "")
            let adr_one = (address["address_one"] as? String ?? "")
            let locality = (address["locality"] as? String ?? "")
            let city = (address["city"] as? String ?? "")
            let zip = (address["zip_code"] as? String ?? "")
            
            self.lblAddress.text = house + " " + adr_one + " " + locality + " " + city + " - " + zip
            
        }
        
        self.lblOrderID.text = "Order Id: " + String(describing: (data["order_id"] as? Int ?? 0))
        self.lblOrderDate.text = data["order_date"] as? String ?? ""
        self.lblOrderTime.text = "Between " + ((data["time_slot"] as? String ?? "").replacingOccurrences(of: "-", with: "to"))
        
        guard let price = data["final_amount"] as? String else {
            self.lblGrandTotal.isHidden = true
            self.lblTitleGrandTotal.isHidden = true
            return
        }
        
            if price == "0.00" {
                self.lblGrandTotal.isHidden = true
                self.lblTitleGrandTotal.isHidden = true
            }
            else {
                self.lblGrandTotal.isHidden = false
                self.lblTitleGrandTotal.isHidden = false
                self.lblGrandTotal.text = "Rs. " + (data["final_amount"] as? String ?? "0.0")
        }
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        popToHome()
    }
    
}

extension GrandTotalViewController{
    func popToHome(){
       
        let vcs : [UIViewController] = self.navigationController!.viewControllers
        
        for viewController in vcs {
            if viewController is HomeViewController{
                self.navigationController?.popToViewController(viewController, animated: true)
            }
        }
    }
}
