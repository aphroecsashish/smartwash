//
//  BaseNavigationViewController.swift
//  SmartWash
//
//  Created by AphroECS on 17/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
