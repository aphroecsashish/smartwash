//
//  HelpViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON

class HelpViewController: UIViewController {

    var orderID: Int?
    
    lazy var middleware: APIClient = APIClient.init(withDelegate: self)
    
    var isFromSideMenu: Bool = false
    
    @IBOutlet weak var helpView: UILabel!
    @IBOutlet weak var tfMobile: SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfhelp: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfName.isUserInteractionEnabled = false
        self.tfEmail.isUserInteractionEnabled = false
        self.tfMobile.isUserInteractionEnabled = false
        
//        tfhelp.text = "Write your comments with order id"
//        tfhelp.textColor = UIColor.lightGray
//        tfhelp.font = UIFont(name: "verdana", size: 13.0)
//        tfhelp.returnKeyType = .done
        
        tfhelp.delegate = self
        
        help()
        hideKeyboardWhenTappedAround()
        self.tfhelp.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
//     func textViewDidBeginEditing(_ textView: UITextView){
//
//        if textView.text == "Write your comments with order id" {
//            textView.text = ""
//            textView.textColor = UIColor.black
//            textView.font = UIFont(name: "verdana", size: 13.0)
//        }
//    }
//
//        func textView(_ textView: UITextView, shouldChangeTextIn: NSRange, replacementText text: String) -> Bool{
//
//            if text == "\n"{
//                textView.resignFirstResponder()
//            }
//            return true
//
//    }
//
    
        
    
    
    func help() {
        
        let bearer = "Bearer"
        //let access_token = UserDefaults.standard.bool(forKey: "token")
        guard let access_token = UserDefaults.standard.value(forKey: "token") as? String else {return}
        print(access_token)
        let bearerString = "\(bearer) \( access_token)"
        print(bearerString)
        
        let urlString = "https://surender.aphroecs.com/smartwash/public/api/user/get_profiles"
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.setValue("\(bearerString)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.get.rawValue
        

        
        Alamofire.request(request).responseJSON{
            
            (response) in
            
            switch response.result {
                
            case .success(_):
//                let res = response.result.value
//                print(res as Any)
                
                let res = JSON(response.result.value!)
                print(res)
                
               
                guard let email1 = res["response"]["data"]["email"].rawValue as? String else {
                    print("token not found")
                    return
                }
                
                guard let mobile1 = res["response"]["data"]["mobile"].rawValue as? String else {
                    print("token not found")
                    return
                }
                
                guard let name1 = res["response"]["data"]["name"].rawValue as? String else {
                    print("token not found")
                    return
                }
                
                print(email1)
                print(mobile1)
                print(name1)
                
                self.tfEmail.isUserInteractionEnabled = false
                self.tfEmail.text = email1
                self.tfMobile.isUserInteractionEnabled = false
                self.tfMobile.text = mobile1
                self.tfName.isUserInteractionEnabled = false
                self.tfName.text = name1
                
            case .failure(_):
                print("failed")
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if self.orderID != nil {
            self.tfhelp.text = "Order Id :\(self.orderID!), write your comments"
        }
        else {
            self.tfhelp.text = "write your comments"
        }
        
        addBarWithBackButton(title: "Help & Support")
        
        if isFromSideMenu {
            self.helpView.isHidden = true
        }
        else {self.helpView.isHidden = false}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func displayMyAlertMessage(userMessage:String){
        
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @IBAction func sendRequest(_ sender: Any) {
        
        dismissKeyboard()
        
        if tfhelp.text.count == 0 || tfhelp.text == "write your comments" {
            
            displayMyAlertMessage(userMessage: "This field cannot be left empty")
            
        } else {
        
        guard let email = tfEmail.text, email.isValidEmail else {
            self.view.makeToast("Please enter correct email", duration: 3.0, position: .bottom)
            return
        }
        
        guard let name = tfName.text, name.isValidName else {
            self.view.makeToast("Please enter correct name", duration: 3.0, position: .bottom)
            return
        }
        
        guard let mobile = tfMobile.text, mobile.isValidPhoneNumber else {
            self.view.makeToast("Please enter correct mobile number", duration: 3.0, position: .bottom)
            return
        }
        
        guard let helpText = tfhelp.text else {
            self.view.makeToast("Please enter correct comment.", duration: 3.0, position: .bottom)
            return}

        self.showIndicator(withMessage: "Sending..")
        
        let params = ["name":name,"email":email,"mobile":mobile,"user_query":helpText] as [String:AnyObject]
        
        middleware.helpAndSupport(params: params)
    }
    }
}

extension HelpViewController: UITextViewDelegate, HTTPDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "write your comments" {
            textView.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if self.orderID != nil {
        if textView.text.count == ("write your comments".count + "\(self.orderID)".count + 1) {
            textView.text.append(" ")
            return false
            }
        else { return true }
        }
        return true
    }
    

func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
    
    hideIndicator()
    
    switch status {
    case .success:
        self.navigationController?.popViewController(animated: true)
    default:
        self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
    }
}
}
