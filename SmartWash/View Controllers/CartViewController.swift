//
//  CartViewController.swift
//  SmartWash
//
//  Created by Ashish sharma on 08/10/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import CoreData

class CartViewController: UIViewController {
    
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var billingDetailsView: UIView!
    
    @IBOutlet weak var LblGrandTotal: UILabel!
    @IBOutlet weak var LblDiscount: UILabel!
    @IBOutlet weak var LblGST: UILabel!
    @IBOutlet weak var LblTotal: UILabel!
    
    var finalAmount: Double = 0.0
    var gst: Double = 0.0
    var discount: Double = 0.0
    var orderAmount: Double = 0.0
    var checkoutProducts: [[String:AnyObject]] = []
    
    var discountResponse: DiscountResponse?
    
    var cartItems: [NSManagedObject] = []
    var products: [Product] = []

    var discountPercent: Double = 0.0
    var GSTPercent: Double = 0.0
    
    let manager = CoreDataManager.sharedManager
    
    lazy var middleWare: APIClient = APIClient.init(withDelegate: self)
    
    enum requestType {
        case requestPickup
        case fetchDiscount
    }
    
    var currentRequestType: requestType = .fetchDiscount
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        manager.delegate = self
        
        self.billingDetailsView.isHidden = true
        cartTableView.register(ListTableViewCell.self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        addCartNavigationBar()
        self.emptyView.isHidden = true
        self.cartTableView.isHidden = false
        self.confirmBtn.isHidden = false
        self.billingDetailsView.isHidden = false
        
        self.cartItems = manager.getCartFromDB()
        self.setupPrice()
        initialiseProduct()
        
        self.currentRequestType = .fetchDiscount
        showIndicator()
        middleWare.fetchDiscounts()
    }
    
    @IBAction func confirmTapped(_ sender: Any) {

        self.currentRequestType = .requestPickup
        
        let vc = SchedulePickupViewController.instantiate(fromStoryboard: .main)
        
        vc.pickup = .normal
        
        vc.finalAmount = self.finalAmount
        vc.gst = self.gst
        vc.discount = self.discount
        vc.orderAmount = self.orderAmount
        vc.products = self.checkoutProducts
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func initialiseProduct() {
        self.products = []
        for prod in cartItems {
            let product = Product.init(withNSManaged: prod)
            self.products.append(product)
            
            let price = Double(product.count ?? 0) * product.price
            
            let item = ["service_id":product.serviceType?.rawValue,"product_id":product.id,"total_price":price,"quantity":product.count] as! [String:AnyObject]
            
            self.checkoutProducts.append(item)
        }}
    
    @IBAction func continueShoppingTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupPrice(){
        let price = manager.getCartAmount()
        self.LblTotal.text = price.RsString
    }
    
    func compareSlab(discountResponse: DiscountResponse, cartValue: Double) -> Double {

        let maximumDiscount = FindMAx(data: discountResponse).1
        let maxSlab = FindMAx(data: discountResponse).0
        
        let slabs = discountResponse.discountData
        
        for slab in slabs {
            
            let min = String(describing: slab.slabFrom).toDouble
            let max = String(describing: slab.slabTo).toDouble
            if cartValue >= min && cartValue <= max {
                return String(describing: slab.discount).toDouble
            }
            else if cartValue > maxSlab {
                return maximumDiscount
            }
        }
        return 0.0
    }
    
    
    func FindMAx(data: DiscountResponse ) -> (Double,Double) {
    
        let slabs = data.discountData
        
        var max = slabs.first!.slabTo
        
        var maxDiscount = 0.0
        
        for slab in slabs {
        if slab.slabTo > max
        {
            max = slab.slabTo
            maxDiscount = String(describing: slab.discount).toDouble
        }
    }
        return (String(describing: max).toDouble,maxDiscount)
    }

    func CalculateDiscount(price: Double) -> Double {

        let discount = compareSlab(discountResponse: self.discountResponse!, cartValue: price)
        
        if discount > 0.0 {
            
            let discountedPrice = (price / 100.0) * discount
            self.LblDiscount.text = String(describing: discountedPrice)
            return discountedPrice
        }
        else { return 0.0 }
    }

    func calculateGST(cartValue: Double, GSTRate: Double) -> Double {
        let GSTPrice = ( cartValue / 100.0 ) * GSTRate
        return GSTPrice
    }

    func totalPrice() {
       
        let price = manager.getCartAmount()
        
        self.orderAmount = price
        
        let gst = calculateGST(cartValue: price, GSTRate: self.discountResponse?.gst ?? 0.0)
        
        self.gst = gst
        
        let discount = CalculateDiscount(price: price)
        
        self.discount = discount
        
        let total = price + gst - discount
        
        self.finalAmount = total
        
        self.LblTotal.text = price.RsString
        self.LblDiscount.text = discount.RsString
        self.LblGST.text = gst.RsString
        self.LblGrandTotal.text = total.RsString
    }
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell") as! ListTableViewCell
        
        cell.delegate = self
        
        let product = products[indexPath.row]
        
        cell.cellProduct = product
        cell.selectedServiceType = product.serviceType
        
        cell.isFromCart = true
        
        cell.initData(product: product)
        cell.categoryType.isHidden = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }}


extension CartViewController: ListTableViewCellDelegate {
    func shouldReload(shouldReload reload: Bool) {
        
    }
    
    
    func valueChanged(forProduct updatedProduct: Product, section: Int, row: Int, shouldRemove: Bool?) {
        
        if updatedProduct.serviceType != nil {
        
        manager.updateCart(product: updatedProduct, serviceType: updatedProduct.serviceType!, shouldRemove: shouldRemove)
            self.cartItems = manager.getCartFromDB()
            
            if cartItems.count > 0 {
            self.initialiseProduct()
            self.setupPrice()
            self.cartTableView.reloadData()
            }
            else {
                self.emptyView.isHidden = false
                self.cartTableView.isHidden = true
                self.confirmBtn.isHidden = true
                self.billingDetailsView.isHidden = true
            }
        }
        self.totalPrice()
    }
}

extension CartViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        hideIndicator()
        switch status {
            
        case .success:
            if  self.currentRequestType == .fetchDiscount {
                
                guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
                    self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                    return
                }
                self.discountResponse = DiscountResponse.init(withJson: data)
                self.totalPrice()
                self.billingDetailsView.isHidden = false
            }
                
            else {
                print("done")
            }
        default: print("something else.")
        }
    }
}

extension CartViewController: CoreDataManagerDelegate {
    func cartUpdated(withProduct: Product) {}
    
    func cartCleared() {
        
        self.navigationItem.setRightBarButton(nil, animated: true)
        self.cartItems = []
        self.cartTableView.reloadData()
        self.emptyView.isHidden = false
        self.cartTableView.isHidden = true
        self.confirmBtn.isHidden = true
        self.billingDetailsView.isHidden = true
    }}
