//
//  SchedulePickupViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SchedulePickupViewController: UIViewController {

    enum activePicker {
        case date
        case time
        case timePeriod
    }
    
    enum requestType {
        case getTimeSlots
        case getAddresses
        case confirm
        case getDateSlots
    }
    
    enum pickupType {
        case express
        case normal
    }
    
    var finalAmount: Double = 0.0
    var gst: Double = 0.0
    var discount: Double = 0.0
    var orderAmount: Double = 0.0
    var products: [[String:AnyObject]] = []
    
    
    var previousCell : IndexPath?
    
    var pickup: pickupType = .express
    var requestedType: requestType = .getAddresses
    
    var addresses: [Address] = []
    var dateSlotsArray: [DateSlot] = []//TimeSlot
     var timeSlotArray: [TimeSlot] = []
    var activePickerType: activePicker = .date
    lazy var middleware = APIClient.init(withDelegate: self)
    var selectedAddress: Address?
    var selectedTime = ""
    var selectedDate = ""
    var comments = ""
    var dateSendForApi = ""
    var selectedTimeSlotID: Int?
    let myPickerView = UIPickerView()
    
    @IBOutlet weak var addressesTableView: UITableView!
    @IBOutlet weak var tfSelectTime: SkyFloatingLabelTextField!
    @IBOutlet weak var tfComments: SkyFloatingLabelTextField!
    @IBOutlet weak var tfSelectDate: SkyFloatingLabelTextField!
    @IBOutlet weak var tfActivityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        self.myPickerView.isHidden = true
        self.tfSelectDate.inputView = myPickerView
        self.tfSelectTime.inputView = myPickerView
        
        self.tfSelectTime.delegate = self
        self.tfSelectDate.delegate = self
        self.tfSelectTime.isUserInteractionEnabled = false
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        addBarWithBackButton(title: "Schedule Pick-up")
        showIndicator()
        self.requestedType = .getAddresses
        middleware.fetchAddresses()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addAddressTapped() {
       
        let vc = NewAddressViewController.instantiate(fromStoryboard: .profile)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func confirmTapped(_ sender: Any) {
        
        dismissKeyboard()
        
        let userID = UserDefaults.standard.integer(forKey: "userID")
        
        if self.selectedDate == "" {
            self.view.makeToast("Please select a date", duration: 3.0, position: .bottom)
            return
        }
        
        if self.selectedTimeSlotID == nil {
            self.view.makeToast("Please select a time", duration: 3.0, position: .bottom)
            return
        }
        
        if self.selectedAddress == nil {
            self.view.makeToast("Please select an address", duration: 3.0, position: .bottom)
            return
        }
        
        if tfComments.text?.count == 0 {
            
            let yesAction = UIAlertAction.init(title: "Yes", style: .default) { _ in
                self.showIndicator()
                self.requestedType = .confirm
                
                if self.pickup == .express
                {
                    self.middleware.confirmPickup(userID: userID, requestedPickupDate: self.selectedDate, addressID: self.selectedAddress!.id,timeSlot: self.selectedTimeSlotID!)
                }
                else {
                    self.middleware.confirmPickup(express: false, userID: userID, requestedPickupDate: self.selectedDate, addressID: self.selectedAddress!.id, timeSlot: self.selectedTimeSlotID!, finalAmount: self.finalAmount, gst: self.gst, discount: self.discount, orderAmount: self.orderAmount, products: self.products)
                }
            }
            
            let noAction = UIAlertAction.init(title: "No", style: .default) { _ in
            }
            
            if self.selectedAddress == nil {
                alert(withTitle: "Alert", desc: "Do you want to proceed without adding comments and with default address?", andActions: [noAction,yesAction])
            }
            else {
                alert(withTitle: "Alert", desc: "you are confirming your order without comment and with default address", andActions: [noAction,yesAction]) }
        }else if (tfComments.text?.count)! > 0{
            let noAction = UIAlertAction.init(title: "No", style: .default) { _ in
            }
            let yesAction = UIAlertAction.init(title: "yes", style: .default) { _ in
                self.showIndicator()
                self.requestedType = .confirm
                
                if self.pickup == .express
                {
                    self.middleware.confirmPickup(userID: userID, requestedPickupDate: self.selectedDate, addressID: self.selectedAddress!.id,timeSlot: self.selectedTimeSlotID!)
                }
                else {
                    self.middleware.confirmPickup(express: false, userID: userID, requestedPickupDate: self.selectedDate, addressID: self.selectedAddress!.id, timeSlot: self.selectedTimeSlotID!, finalAmount: self.finalAmount, gst: self.gst, discount: self.discount, orderAmount: self.orderAmount, products: self.products)
                }
                
            }
            alert(withTitle: "Alert", desc: "you are confirming your address with default address", andActions: [noAction,yesAction])
            
        }else {
            self.showIndicator()
            self.requestedType = .confirm
            
            if self.pickup == .express
            {
                self.middleware.confirmPickup(userID: userID, requestedPickupDate: self.selectedDate, addressID: self.selectedAddress!.id,timeSlot: self.selectedTimeSlotID!)
            }
            else {
                self.middleware.confirmPickup(express: false, userID: userID, requestedPickupDate: self.selectedDate, addressID: self.selectedAddress!.id, timeSlot: self.selectedTimeSlotID!, finalAmount: self.finalAmount, gst: self.gst, discount: self.discount, orderAmount: self.orderAmount, products: self.products)
            }

        }
        
    }
    
}

extension SchedulePickupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.addresses.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == self.addresses.count {
            return 50
        }
        else {
        return 80
        }}
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 9.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == self.addresses.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addAddress")
            return cell!
        }
            
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowAddressCell") as! ShowAddressTableViewCell
        
        let address = self.addresses[indexPath.section]
        
        cell.AddressLabel.text = address.house + ", " + address.locality + ", "  + address.city + "," + address.zipCode
        
        let isDefault = Bool.init(exactly: NSNumber.init(value: address.isDefault))
        
        if isDefault! {
            
            self.addressesTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            self.selectedAddress = addresses[indexPath.section]
        }
        else {
            cell.setSelected(false, animated: true)
        }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == self.addresses.count {
            self.addAddressTapped()
        } else {
        self.selectedAddress = self.addresses[indexPath.section]
        }}
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowAddressCell") as! ShowAddressTableViewCell
        cell.selectionStyle = .none
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
}


extension SchedulePickupViewController: UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch self.activePickerType {
        case .date:
            return self.dateSlotsArray.count
        default:
            return self.timeSlotArray.count
        }
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.myPickerView.isHidden = false

        if textField == tfSelectDate {
            self.tfSelectTime.isUserInteractionEnabled = false
            self.tfSelectTime.text = ""
          //  self.myPickerView.datePickerMode = .date
            //self.myPickerView.minimumDate = Date()
            self.selectedTimeSlotID = nil
            self.activePickerType = .date
        }
        if textField == tfSelectTime {
            //picker.datePickerMode = .time
            //self.myPickerView.minimumDate = Date()
            self.activePickerType = .time
        }
    }
    

    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        switch activePickerType {
        case .date:
            let dateComponent = self.dateSlotsArray[row].viewDate
            return dateComponent
       case .time:
        let timeComponent = self.timeSlotArray[row].timeSlot
        
        return timeComponent

        case .timePeriod:
            return ""
         }
        }
        
   
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch self.activePickerType {
        case .date:
            self.tfSelectDate.text = self.dateSlotsArray[row].viewDate
            self.dateSendForApi = self.dateSlotsArray[row].date
            self.selectedDate = self.dateSlotsArray[row].date
            self.activePickerType = .date
            self.requestedType = .getTimeSlots
            let param : [String:AnyObject] = ["date":dateSendForApi as AnyObject]
            self.tfActivityIndicator.startAnimating()
            self.timeSlotArray = []
            middleware.fetchTimeSlot(params: param)
            
         case .time:
            self.tfSelectTime.text = self.timeSlotArray[row].timeSlot
            self.selectedTimeSlotID = self.timeSlotArray[row].id
    
            break
            
        default :
            break
        }
        
    }
}

extension SchedulePickupViewController {
    
    func handleSuccess(object: AnyObject) {
        
        self.resignFirstResponder()
        
        switch self.requestedType {
            
        case .getAddresses :
            
                guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
                    self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                    return
                }
                
                self.addresses = []
                
                let addresses = data["data"] as! [[String:AnyObject]]
                for addr in addresses {
                    let adr = Address.init(withJSON: addr)
                    self.addresses.append(adr)
                }
                self.addressesTableView.reloadData()
            
            self.requestedType = .getDateSlots
            middleware.fetchdates()
            
            
        case .getTimeSlots :
            
            self.tfActivityIndicator.stopAnimating()
            
            guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
               self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
            }
            self.timeSlotArray = []
            let dateSlots = data["data"] as! [[String:AnyObject]]
            for times in dateSlots {
                let time = TimeSlot.init(withJSON: times)
                self.timeSlotArray.append(time)
            }
            self.tfSelectTime.isUserInteractionEnabled = true
            self.myPickerView.delegate = self
            self.tfSelectDate.resignFirstResponder()
            self.tfSelectDate.inputAccessoryView?.resignFirstResponder()
            self.activePickerType = .time
            self.myPickerView.reloadAllComponents()
            
        case .confirm :
            
            CoreDataManager.sharedManager.clearCart()
            
            let vc = GrandTotalViewController.instantiate(fromStoryboard: .main)
            
            vc.orderDetails = object["response"] as? [String:AnyObject] ?? [:]
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .getDateSlots :
            
            guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
            }
            
            self.dateSlotsArray = []
            let dateSlots = data["data"] as! [[String:AnyObject]]
            for dates in dateSlots {
                let date = DateSlot.init(withJSON: dates)
                self.dateSlotsArray.append(date)
            }
            print(data)
            self.timeSlotArray = []
            self.myPickerView.delegate = self
            self.activePickerType = .date
        }
        
      
    }
    
    func handleError(object: AnyObject) {
        
        tfSelectDate.resignFirstResponder()
        
        self.tfActivityIndicator.stopAnimating()
        
        guard let error = (((object as? [String:AnyObject]) as? NSDictionary)?.value(forKey: "response") as! NSDictionary).value(forKey: "message") as? String
            else {
                self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
                return
        }
        
        self.view.makeToast(error, duration: 3.0, position: .bottom)
    }
}

extension SchedulePickupViewController: HTTPDelegate {
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            
            self.handleSuccess(object: object!)
            
        default:
            
            self.handleError(object: object!)
            
        }
    }
}

extension SchedulePickupViewController: NewAddressViewControllerDelegate {
    
    func addressCreated(address: Address) {
        self.addresses.insert(address, at: 0)
        self.addressesTableView.reloadData()
        self.addressesTableView.selectRow(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .none)
    }
}
