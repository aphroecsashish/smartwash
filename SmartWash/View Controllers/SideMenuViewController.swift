//
//  SideMenuViewController.swift
//  SmartWash
//
//  Created by AphroECS on 31/08/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import SideMenu

class SideMenuViewController: UIViewController {
    
    let menuOptions = ["Home","My Profile","Order History","Feedback","Customer Care","Rate Card","Store Locator","Offers"]
    
    let menuIcons = [#imageLiteral(resourceName: "home (1)"),#imageLiteral(resourceName: "user(7)"),#imageLiteral(resourceName: "order history"),#imageLiteral(resourceName: "FEEDBACK"),#imageLiteral(resourceName: "telemarketer"),#imageLiteral(resourceName: "clipboard"),#imageLiteral(resourceName: "pin"),#imageLiteral(resourceName: "discount")]
    
    @IBOutlet weak var lblName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {

        
        guard let name =  UserDefaults.standard.value(forKey: "name") as? String else {
            self.lblName.text = "Welcome"
            return
        }
        
        self.lblName.text = "Welcome" + " " + name
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        
        CoreDataManager.sharedManager.clearCart()
        
        self.dismiss(animated: true) {
        
        UserDefaults.standard.set(false, forKey: Keys.login)
            UserDefaults.standard.removeObject(forKey: "name")
            UserDefaults.standard.removeObject(forKey: "userID")
        DefaultProperties.appDelegate.showLogin(true, UIViewAnimationOptions.transitionFlipFromLeft, fromLaunch: false)
        }
    }
}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuTableViewCell
        
        cell.menuIcon.image = menuIcons[indexPath.row]
        cell.menutTitle.text = menuOptions[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
        let nav = DefaultProperties.appDelegate.baseNavigationController
        
            guard let topVC = nav.viewControllers.last else { return }
            
        switch indexPath.row {
        case 0:
            
            guard !(topVC is HomeViewController) else {
                return
            }
            
            nav.popToRootViewController(animated: true)
            
        case 1:
            
            guard !(topVC is ProfileViewController) else {
                return
            }
            
            self.performSegue(withIdentifier: "profileVC", sender: self)
            
         
        case 2:
            
            guard !(topVC is OrdersViewController) else {
                return
            }
            
           self.performSegue(withIdentifier: "ordersVC", sender: self)

        case 4: guard !(topVC is HelpViewController) else {
                return
            }
           
            self.performSegue(withIdentifier: "showHelp", sender: self)
            
        default:
            
            let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: { _ in
                
            })
            UIApplication.shared.keyWindow?.rootViewController?.alert(withTitle: "Coming soon", desc: "", andActions: [okAction])
        }}
        dismiss(animated: true, completion: nil)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showHelp" {
            
            let vc = HelpViewController.instantiate(fromStoryboard: .main)
            vc.isFromSideMenu = true
        }
        return true
    }
}
