//
//  DeliveryDetailsViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit

class DeliveryDetailsViewController: UIViewController {

    @IBOutlet weak var orderDetailsTable: UITableView!
   
    var orderID: Int!
    
    var products: [[String:AnyObject]] = []
    
    var data: [String:AnyObject]?
    
    lazy var middleWare: APIClient = APIClient.init(withDelegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        addBarWithBackButton(title: "Order ID: \(orderID!)")
        
        showIndicator()
        middleWare.fetchOrder(orderID: orderID)
    }
    @IBAction func helpTapped(_ sender: Any) {
        
        let vc = HelpViewController.instantiate(fromStoryboard: .main)
        
        vc.orderID = self.orderID
        
        self.navigationController?.pushViewController(vc, animated: true)

    }
}

extension DeliveryDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
   
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Order Details"
        }
        else {
            return "" }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        }
        else {
            return 40
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.products.isEmpty ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return self.products.count
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! OrderDetailsTableViewCell
        
            if nil != self.data{
                    cell.configure(withObject: self.data!)
            }
            
            return cell
            
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! DeliveryDetailsProductCell
            
            let product = self.products[indexPath.row]
            
            var text = (product["name"] as? String ?? "") + " ( " + (product["category"] as? String ?? "")
            
            text += " )" + " x " + String(describing: (product["quantity"] as? Int ?? 0 ))
            
            cell.productName.text = text
            
            cell.productPrice.text = "Rs. " + (product["price"] as? String ?? "0.0")
            
            cell.serviceType.text = product["serviceType"] as? String ?? ""
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 340 : 75
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
}

extension DeliveryDetailsViewController: HTTPDelegate {
    
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            handleResponse(withObject: object!)
        default:
            handleError(withObject: object ?? "" as AnyObject)
        }
    }
    
    func handleResponse(withObject object: AnyObject?) {
        
        guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
            self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        let dData = data["data"] as! [String:AnyObject]
     
        self.data = dData
        
        if let productsRsp = dData["order_detail_products"] as? [[String:AnyObject]]
        {
            self.products = []
            
            for prod in productsRsp {
                
                guard let productNameA = prod["products"] as? [String:AnyObject] else {return}
                
                let prodName = productNameA["name"] as? String ?? ""
                
                let serviceType = (prod["service_id"] as? String ?? "").replacingOccurrences(of: "_", with: " ")
                
                let quantity = prod["quantity"] as? Int ?? 1
                
                let price = prod["total_price"] as? String ?? ""
                
                let category = prod["category"] as? String ?? ""
                
                var arr = [:] as! [String:AnyObject]
                
                arr = ["category":category,"name":prodName,"serviceType":serviceType,"quantity":quantity,"price":price] as [String : AnyObject]
                
                self.products.append(arr)
            }
        }
        self.orderDetailsTable.reloadData()
        print(dData)
    }
    
    func handleError(withObject object: AnyObject?) {
        
        guard let ob = object?["data"] as? [String:AnyObject] else {
            return
        }
        
        guard let msg = ob["msg"] as? String else {
            return
        }
        
        self.view.makeToast(msg)
    }
}

