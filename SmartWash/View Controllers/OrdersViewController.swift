//
//  OrdersViewController.swift
//  SmartWash
//
//  Created by AphroECS on 13/09/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import SideMenu

class OrdersViewController: UIViewController {

    lazy var middleWare: APIClient = APIClient.init(withDelegate: self)
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var ordersTable: UITableView!
    
    var orders: [Order] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        showIndicator()
        
        addBarWithBackButton(title: "Orders")
        
        let userID = UserDefaults.standard.integer(forKey: "userID")
        
        middleWare.fetchOrders(userID: userID)
        
        self.navigationController?.navigationItem.setHidesBackButton(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension OrdersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.orders.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as? OrderTableViewCell
        
        let order = orders[indexPath.section]
        
        cell?.configure(withOrder: order)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let order = orders[indexPath.section]
        
        if order.status == "cancelled" {  }
        else {
            
            let vc = DeliveryDetailsViewController.instantiate(fromStoryboard: .main)
            vc.orderID = order.id
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        return view
    }
    
}


extension OrdersViewController: HTTPDelegate {
    
    func completed(withStatus status: HTTPStatus, responseObject object: AnyObject?, andErrorMsg msg: String) {
        
        hideIndicator()
        
        switch status {
        case .success:
            handleResponse(withObject: object!)
        default:
            handleError(withObject: object ?? "" as AnyObject)
        }
    }
    
    func handleResponse(withObject object: AnyObject?) {
        
        guard let data = ((object as! NSDictionary).value(forKey: "response") as? [String:AnyObject]) else {
            self.view.makeToast("Some error occured", duration: 3.0, position: .bottom)
            return
        }
        
        let orders = data["data"] as! [[String:AnyObject]]
        
        if orders.count == 0 {
            self.emptyView.isHidden = false
            self.ordersTable.isHidden = true
        }
        else {
            self.emptyView.isHidden = true
            self.ordersTable.isHidden = false
        }
        
        self.orders = []
        
        for order in orders {
            self.orders.append(Order.init(withJson: order))
        }
        
        self.ordersTable.reloadData()
        
    }
    
    func handleError(withObject object: AnyObject?) {}
    
}
