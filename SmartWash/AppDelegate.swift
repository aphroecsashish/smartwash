//
//  AppDelegate.swift
//  SmartWash
//
//  Created by AphroECS on 29/08/18.
//  Copyright © 2018 AphroEcom. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    lazy var baseNavigationController = BaseNavigationViewController.instantiate(fromStoryboard: .main)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //DropDown.startListeningToKeyboard()
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "headerBg")!.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch), for: .default)
        
        
        IQKeyboardManager.shared().isEnabled = true
        
        setRootViewController()
        
        return true
    }
    
    func setRootViewController() {
        
        UserDefaults.standard.bool(forKey: Keys.login) ? showHome() : showLogin()
        
    }
    
    
    func showLogin(_ animated: Bool = false,_ animation: UIViewAnimationOptions = .transitionFlipFromLeft,fromLaunch: Bool = true) {
        
        let navigationController = LoginNavigationController.instantiate(fromStoryboard: .login)
        
        if !fromLaunch {
            let loginNav = LoginViewController.instantiate(fromStoryboard: .login)
            navigationController.pushViewController(loginNav, animated: true)
        }
        
        if animated {
            
            UIView.transition(with: window!, duration: 0.3, options: animation, animations: {
                self.window?.rootViewController = navigationController
            }, completion: nil)
        } else {
            self.window?.rootViewController = navigationController
        }
    }

func showHome(_ animated: Bool = false, _ animation: UIViewAnimationOptions = .transitionFlipFromLeft) {
    
    baseNavigationController = BaseNavigationViewController.instantiate(fromStoryboard: .main)
    
    guard let window = window else {return}
    if animated {
        UIView.transition(with: window, duration: 0.3, options: animation, animations: {
            window.rootViewController = self.baseNavigationController
        }, completion: nil)
        
    } else {
        window.rootViewController = baseNavigationController
    }
}

func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}
}

